package com.sda.Gina.sudentRegister.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class StudentRegisterMainController implements Initializable {
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    private void showStudentRegisterLoginScene(ActionEvent event){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(new URL("file:C:\\Users\\Gina\\IdeaProjects\\StudentRegister\\src\\main\\java\\com\\sda\\Gina\\sudentRegister\\view\\StudentRegisterLogin.fxml"));
            Parent loginParent = loader.load();

            Scene scene = new Scene(loginParent);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

            window.setScene(scene);
            window.centerOnScreen();
            window.setTitle("Student and Trainer Login form");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
