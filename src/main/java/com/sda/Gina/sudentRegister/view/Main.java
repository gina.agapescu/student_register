package com.sda.Gina.sudentRegister.view;

import com.sda.Gina.sudentRegister.model.*;
import com.sda.Gina.sudentRegister.repository.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {

    static MaintainerDao maintainerDao = new MaintainerHibernate();
    static StudentDao studentDao = new StudentHibernate();
    static TrainerDao trainerDao = new TrainerHibernate();
    static ModuleDao moduleDao = new ModuleHibernate();
    static CourseDao courseDao = new CourseHibernate();
    static TopicDao topicDao = new TopicHibernate();
    static AttendanceDao attendanceDao = new AttendanceHibernate();
    static MarkDao markDao = new MarkHibernate();

    public static void main(String[] args) {
	saveMaintainer();
	displayInfoMaintainer();

	//saveModule();
     saveCourse();

	//displayInfoTrainer();
	//displayInfoModule();

    //displayInfoStudent();
	//displayInfoTopic();

    saveMark();
    //displayInfoMark();

	saveAttendanceHistory();
	saveAttendanceOperators();
	saveAttendanceArray();
	saveAttendanceControlFlow();
	saveAttendanceStrings();
	saveAttendanceVariable();
	saveAttendanceCoding1();
	saveAttendanceCoding2();
	saveAttendanceCoding3();
	saveAttendanceOop();
	saveAttendanceTesting();
	saveAttendanceVarargs();
    displayInfoAttendance();
    displayInfoCourse();




	//searchTrainerById();
	//searchTrainerByNamePasswordAndStatus();
	//displayInfoCourse();
    }

    public static void saveMark(){
        Module module = moduleDao.readModuleById(1L);
        Course course = courseDao.readCourseById(1L);
        Mark markIoana = new Mark();
        markIoana.setModule(module);
        markIoana.setCourse(course);
        markIoana.setDate(LocalDate.of(2019, 11, 24));
        markIoana.setStudent(studentDao.readStudentByLastName("Secan"));
        markIoana.setNote(9);

        Mark markRaul = new Mark();
        markRaul.setModule(module);
        markRaul.setCourse(course);
        markRaul.setDate(LocalDate.of(2019, 11, 24));
        markRaul.setStudent(studentDao.readStudentByLastName("Opris"));
        markRaul.setNote(8);

        Mark markCipri = new Mark();
        markCipri.setModule(module);
        markCipri.setCourse(course);
        markCipri.setDate(LocalDate.of(2019, 11, 24));
        markCipri.setStudent(studentDao.readStudentByLastName("Cornea"));
        markCipri.setNote(7);

        Mark markCorina = new Mark();
        markCorina.setModule(module);
        markCorina.setCourse(course);
        markCorina.setDate(LocalDate.of(2019, 11, 24));
        markCorina.setStudent(studentDao.readStudentByLastName("Costea"));
        markCorina.setNote(8);

        Mark markGina = new Mark();
        markGina.setCourse(course);
        markGina.setModule(module);
        markGina.setDate(LocalDate.of(2019, 11, 24));
        markGina.setStudent(studentDao.readStudentByLastName("Agapescu"));
        markGina.setNote(10);

        Mark markCostin = new Mark();
        markCostin.setCourse(course);
        markCostin.setModule(module);
        markCostin.setDate(LocalDate.of(2019, 11, 24));
        markCostin.setStudent(studentDao.readStudentByLastName("Branzas"));
        markCostin.setNote(10);

        markDao.createMark(markCipri);
        markDao.createMark(markCorina);
        markDao.createMark(markCostin);
        markDao.createMark(markGina);
        markDao.createMark(markIoana);
        markDao.createMark(markRaul);
    }

    public static void displayInfoMark(){
        System.out.println("---------------MARK INFO--------------");
        for(Mark mark : markDao.readAllMark()){
            mark.displayInfoMark();
            System.out.println();
        }
        System.out.println("--------------------------------------");
    }

    public static void saveAttendanceHistory(){
        Topic topic = topicDao.readTopicById(1L);
        Course course = courseDao.readCourseById(1L);

        Attendance attendanceGina = new Attendance();
        attendanceGina.setDate(LocalDate.of(2019, 10, 13));
        attendanceGina.setStudent(studentDao.readStudentByLastName("Agapescu"));
        attendanceGina.setPresent(true);
        attendanceGina.setCourse(course);
        attendanceGina.setTopic(topic);

        Attendance attendanceCorina = new Attendance();
        attendanceCorina.setDate(LocalDate.of(2019, 10, 13));
        attendanceCorina.setStudent(studentDao.readStudentByLastName("Costea"));
        attendanceCorina.setPresent(true);
        attendanceCorina.setTopic(topic);
        attendanceCorina.setCourse(course);

        Attendance attendanceCostin = new Attendance();
        attendanceCostin.setDate(LocalDate.of(2019, 10, 13));
        attendanceCostin.setStudent(studentDao.readStudentByLastName("Branzas"));
        attendanceCostin.setPresent(true);
        attendanceCostin.setTopic(topic);
        attendanceCostin.setCourse(course);

        Attendance attendanceIoana = new Attendance();
        attendanceIoana.setDate(LocalDate.of(2019, 10,13));
        attendanceIoana.setStudent(studentDao.readStudentByLastName("Secan"));
        attendanceIoana.setPresent(true);
        attendanceIoana.setTopic(topic);
        attendanceIoana.setCourse(course);

        Attendance attendanceRaul = new Attendance();
        attendanceRaul.setDate(LocalDate.of(2019, 10, 13));
        attendanceRaul.setStudent(studentDao.readStudentByLastName("Opris"));
        attendanceRaul.setPresent(true);
        attendanceRaul.setTopic(topic);
        attendanceRaul.setCourse(course);

        Attendance attendanceCipri = new Attendance();
        attendanceCipri.setDate(LocalDate.of(2019, 10, 13));
        attendanceCipri.setStudent(studentDao.readStudentByLastName("Cornea"));
        attendanceCipri.setPresent(true);
        attendanceCipri.setTopic(topic);
        attendanceCipri.setCourse(course);

        List<Attendance>attendanceList = new ArrayList<>();
        attendanceList.add(attendanceCipri);
        attendanceList.add(attendanceCorina);
        attendanceList.add(attendanceCostin);
        attendanceList.add(attendanceGina);
        attendanceList.add(attendanceIoana);
        attendanceList.add(attendanceRaul);

        for(Attendance attendance:attendanceList){
            attendanceDao.createAttendance(attendance);
        }

    }

    public static void saveAttendanceVariable(){
        Topic topic = topicDao.readTopicById(2L);
        Course course = courseDao.readCourseById(1L);

        Attendance attendanceGina = new Attendance();
        attendanceGina.setDate(LocalDate.of(2019, 10, 14));
        attendanceGina.setStudent(studentDao.readStudentByLastName("Agapescu"));
        attendanceGina.setPresent(true);
        attendanceGina.setCourse(course);
        attendanceGina.setTopic(topic);

        Attendance attendanceCorina = new Attendance();
        attendanceCorina.setDate(LocalDate.of(2019, 10, 14));
        attendanceCorina.setStudent(studentDao.readStudentByLastName("Costea"));
        attendanceCorina.setPresent(true);
        attendanceCorina.setTopic(topic);
        attendanceCorina.setCourse(course);

        Attendance attendanceCostin = new Attendance();
        attendanceCostin.setDate(LocalDate.of(2019, 10, 14));
        attendanceCostin.setStudent(studentDao.readStudentByLastName("Branzas"));
        attendanceCostin.setPresent(true);
        attendanceCostin.setTopic(topic);
        attendanceCostin.setCourse(course);

        Attendance attendanceIoana = new Attendance();
        attendanceIoana.setDate(LocalDate.of(2019, 10,14));
        attendanceIoana.setStudent(studentDao.readStudentByLastName("Secan"));
        attendanceIoana.setPresent(true);
        attendanceIoana.setTopic(topic);
        attendanceIoana.setCourse(course);

        Attendance attendanceRaul = new Attendance();
        attendanceRaul.setDate(LocalDate.of(2019, 10, 14));
        attendanceRaul.setStudent(studentDao.readStudentByLastName("Opris"));
        attendanceRaul.setPresent(true);
        attendanceRaul.setTopic(topic);
        attendanceRaul.setCourse(course);

        Attendance attendanceCipri = new Attendance();
        attendanceCipri.setDate(LocalDate.of(2019, 10, 14));
        attendanceCipri.setStudent(studentDao.readStudentByLastName("Cornea"));
        attendanceCipri.setPresent(true);
        attendanceCipri.setTopic(topic);
        attendanceCipri.setCourse(course);

        List<Attendance>attendanceList = new ArrayList<>();
        attendanceList.add(attendanceCipri);
        attendanceList.add(attendanceCorina);
        attendanceList.add(attendanceCostin);
        attendanceList.add(attendanceGina);
        attendanceList.add(attendanceIoana);
        attendanceList.add(attendanceRaul);

        for(Attendance attendance:attendanceList){
            attendanceDao.createAttendance(attendance);
        }

    }

    public static void searchTrainerByNamePasswordAndStatus(){
        Trainer trainer = trainerDao.readTrainerByNamePasswordAndStatus("Noge", "alina", Status.TRAINER);
        System.out.println("Trainer id= "+trainer.getId());
        System.out.println("Name= "+trainer.getFirstName());
        System.out.println("Trainer email= "+ trainer.getEmail());
        System.out.println("Trainer phone= "+trainer.getPhone());
        System.out.println("Trainer is certificate= "+trainer.isCertificate());
        System.out.println("Module= "+trainer.getModule().getModuleName());
    }

    public static void searchTrainerById(){
        Trainer trainer = trainerDao.readTrainerById(2L);
        System.out.println("Name= "+trainer.getFirstName()+" "+trainer.getLastName());
        System.out.println("Trainer email= "+ trainer.getEmail());
        System.out.println("Trainer phone= "+trainer.getPhone());
        System.out.println("Trainer status= "+ trainer.getStatus());
        System.out.println("Trainer password= "+trainer.getPassword());
        System.out.println("Trainer is certificate= "+trainer.isCertificate());
        System.out.println("Module= "+trainer.getModule().getModuleName());
    }

    public static void saveAttendanceOperators(){
        Topic topic = topicDao.readTopicById(3L);
        Course course = courseDao.readCourseById(1L);

        Attendance attendanceGina = new Attendance();
        attendanceGina.setDate(LocalDate.of(2019, 10, 20));
        attendanceGina.setStudent(studentDao.readStudentByLastName("Agapescu"));
        attendanceGina.setPresent(true);
        attendanceGina.setCourse(course);
        attendanceGina.setTopic(topic);

        Attendance attendanceCorina = new Attendance();
        attendanceCorina.setDate(LocalDate.of(2019, 10, 20));
        attendanceCorina.setStudent(studentDao.readStudentByLastName("Costea"));
        attendanceCorina.setPresent(true);
        attendanceCorina.setTopic(topic);
        attendanceCorina.setCourse(course);

        Attendance attendanceCostin = new Attendance();
        attendanceCostin.setDate(LocalDate.of(2019, 10, 20));
        attendanceCostin.setStudent(studentDao.readStudentByLastName("Branzas"));
        attendanceCostin.setPresent(true);
        attendanceCostin.setTopic(topic);
        attendanceCostin.setCourse(course);

        Attendance attendanceIoana = new Attendance();
        attendanceIoana.setDate(LocalDate.of(2019, 10,20));
        attendanceIoana.setStudent(studentDao.readStudentByLastName("Secan"));
        attendanceIoana.setPresent(true);
        attendanceIoana.setTopic(topic);
        attendanceIoana.setCourse(course);

        Attendance attendanceRaul = new Attendance();
        attendanceRaul.setDate(LocalDate.of(2019, 10, 20));
        attendanceRaul.setStudent(studentDao.readStudentByLastName("Opris"));
        attendanceRaul.setPresent(true);
        attendanceRaul.setTopic(topic);
        attendanceRaul.setCourse(course);

        Attendance attendanceCipri = new Attendance();
        attendanceCipri.setDate(LocalDate.of(2019, 10, 20));
        attendanceCipri.setStudent(studentDao.readStudentByLastName("Cornea"));
        attendanceCipri.setPresent(true);
        attendanceCipri.setTopic(topic);
        attendanceCipri.setCourse(course);

        List<Attendance>attendanceList = new ArrayList<>();
        attendanceList.add(attendanceCipri);
        attendanceList.add(attendanceCorina);
        attendanceList.add(attendanceCostin);
        attendanceList.add(attendanceGina);
        attendanceList.add(attendanceIoana);
        attendanceList.add(attendanceRaul);

        for(Attendance attendance:attendanceList){
            attendanceDao.createAttendance(attendance);
        }

    }

    public static void saveAttendanceStrings(){
        Topic topic = topicDao.readTopicById(4L);
        Course course = courseDao.readCourseById(1L);

        Attendance attendanceGina = new Attendance();
        attendanceGina.setDate(LocalDate.of(2019, 10, 21));
        attendanceGina.setStudent(studentDao.readStudentByLastName("Agapescu"));
        attendanceGina.setPresent(true);
        attendanceGina.setCourse(course);
        attendanceGina.setTopic(topic);

        Attendance attendanceCorina = new Attendance();
        attendanceCorina.setDate(LocalDate.of(2019, 10, 21));
        attendanceCorina.setStudent(studentDao.readStudentByLastName("Costea"));
        attendanceCorina.setPresent(true);
        attendanceCorina.setTopic(topic);
        attendanceCorina.setCourse(course);

        Attendance attendanceCostin = new Attendance();
        attendanceCostin.setDate(LocalDate.of(2019, 10, 21));
        attendanceCostin.setStudent(studentDao.readStudentByLastName("Branzas"));
        attendanceCostin.setPresent(true);
        attendanceCostin.setTopic(topic);
        attendanceCostin.setCourse(course);

        Attendance attendanceIoana = new Attendance();
        attendanceIoana.setDate(LocalDate.of(2019, 10,21));
        attendanceIoana.setStudent(studentDao.readStudentByLastName("Secan"));
        attendanceIoana.setPresent(true);
        attendanceIoana.setTopic(topic);
        attendanceIoana.setCourse(course);

        Attendance attendanceRaul = new Attendance();
        attendanceRaul.setDate(LocalDate.of(2019, 10, 21));
        attendanceRaul.setStudent(studentDao.readStudentByLastName("Opris"));
        attendanceRaul.setPresent(true);
        attendanceRaul.setTopic(topic);
        attendanceRaul.setCourse(course);

        Attendance attendanceCipri = new Attendance();
        attendanceCipri.setDate(LocalDate.of(2019, 10, 21));
        attendanceCipri.setStudent(studentDao.readStudentByLastName("Cornea"));
        attendanceCipri.setPresent(true);
        attendanceCipri.setTopic(topic);
        attendanceCipri.setCourse(course);

        List<Attendance>attendanceList = new ArrayList<>();
        attendanceList.add(attendanceCipri);
        attendanceList.add(attendanceCorina);
        attendanceList.add(attendanceCostin);
        attendanceList.add(attendanceGina);
        attendanceList.add(attendanceIoana);
        attendanceList.add(attendanceRaul);

        for(Attendance attendance:attendanceList){
            attendanceDao.createAttendance(attendance);
        }

    }

    public static void saveAttendanceControlFlow(){
        Topic topic = topicDao.readTopicById(5L);
        Course course = courseDao.readCourseById(1L);

        Attendance attendanceGina = new Attendance();
        attendanceGina.setDate(LocalDate.of(2019, 10, 27));
        attendanceGina.setStudent(studentDao.readStudentByLastName("Agapescu"));
        attendanceGina.setPresent(true);
        attendanceGina.setCourse(course);
        attendanceGina.setTopic(topic);

        Attendance attendanceCorina = new Attendance();
        attendanceCorina.setDate(LocalDate.of(2019, 10, 27));
        attendanceCorina.setStudent(studentDao.readStudentByLastName("Costea"));
        attendanceCorina.setPresent(true);
        attendanceCorina.setTopic(topic);
        attendanceCorina.setCourse(course);

        Attendance attendanceCostin = new Attendance();
        attendanceCostin.setDate(LocalDate.of(2019, 10, 27));
        attendanceCostin.setStudent(studentDao.readStudentByLastName("Branzas"));
        attendanceCostin.setPresent(true);
        attendanceCostin.setTopic(topic);
        attendanceCostin.setCourse(course);

        Attendance attendanceIoana = new Attendance();
        attendanceIoana.setDate(LocalDate.of(2019, 10,27));
        attendanceIoana.setStudent(studentDao.readStudentByLastName("Secan"));
        attendanceIoana.setPresent(true);
        attendanceIoana.setTopic(topic);
        attendanceIoana.setCourse(course);

        Attendance attendanceRaul = new Attendance();
        attendanceRaul.setDate(LocalDate.of(2019, 10, 27));
        attendanceRaul.setStudent(studentDao.readStudentByLastName("Opris"));
        attendanceRaul.setPresent(true);
        attendanceRaul.setTopic(topic);
        attendanceRaul.setCourse(course);

        Attendance attendanceCipri = new Attendance();
        attendanceCipri.setDate(LocalDate.of(2019, 10, 27));
        attendanceCipri.setStudent(studentDao.readStudentByLastName("Cornea"));
        attendanceCipri.setPresent(true);
        attendanceCipri.setTopic(topic);
        attendanceCipri.setCourse(course);

        List<Attendance>attendanceList = new ArrayList<>();
        attendanceList.add(attendanceCipri);
        attendanceList.add(attendanceCorina);
        attendanceList.add(attendanceCostin);
        attendanceList.add(attendanceGina);
        attendanceList.add(attendanceIoana);
        attendanceList.add(attendanceRaul);

        for(Attendance attendance:attendanceList){
            attendanceDao.createAttendance(attendance);
        }

    }

    public static void saveAttendanceArray(){
       Topic topic = topicDao.readTopicById(6L);
        Course course = courseDao.readCourseById(1L);

        Attendance attendanceGina = new Attendance();
        attendanceGina.setDate(LocalDate.of(2019, 10, 28));
        attendanceGina.setStudent(studentDao.readStudentByLastName("Agapescu"));
        attendanceGina.setPresent(true);
        attendanceGina.setCourse(course);
        attendanceGina.setTopic(topic);

        Attendance attendanceCorina = new Attendance();
        attendanceCorina.setDate(LocalDate.of(2019, 10, 28));
        attendanceCorina.setStudent(studentDao.readStudentByLastName("Costea"));
        attendanceCorina.setPresent(true);
        attendanceCorina.setTopic(topic);
        attendanceCorina.setCourse(course);

        Attendance attendanceCostin = new Attendance();
        attendanceCostin.setDate(LocalDate.of(2019, 10, 28));
        attendanceCostin.setStudent(studentDao.readStudentByLastName("Branzas"));
        attendanceCostin.setPresent(true);
        attendanceCostin.setTopic(topic);
        attendanceCostin.setCourse(course);

        Attendance attendanceIoana = new Attendance();
        attendanceIoana.setDate(LocalDate.of(2019, 10,28));
        attendanceIoana.setStudent(studentDao.readStudentByLastName("Secan"));
        attendanceIoana.setPresent(true);
        attendanceIoana.setTopic(topic);
        attendanceIoana.setCourse(course);

        Attendance attendanceRaul = new Attendance();
        attendanceRaul.setDate(LocalDate.of(2019, 10, 28));
        attendanceRaul.setStudent(studentDao.readStudentByLastName("Opris"));
        attendanceRaul.setPresent(true);
        attendanceRaul.setTopic(topic);
        attendanceRaul.setCourse(course);

        Attendance attendanceCipri = new Attendance();
        attendanceCipri.setDate(LocalDate.of(2019, 10, 28));
        attendanceCipri.setStudent(studentDao.readStudentByLastName("Cornea"));
        attendanceCipri.setPresent(true);
        attendanceCipri.setTopic(topic);
        attendanceCipri.setCourse(course);

        List<Attendance>attendanceList = new ArrayList<>();
        attendanceList.add(attendanceCipri);
        attendanceList.add(attendanceCorina);
        attendanceList.add(attendanceCostin);
        attendanceList.add(attendanceGina);
        attendanceList.add(attendanceIoana);
        attendanceList.add(attendanceRaul);

        for(Attendance attendance:attendanceList){
            attendanceDao.createAttendance(attendance);
        }

    }

    public static void saveAttendanceOop(){
        Topic topic = topicDao.readTopicById(7L);
        Course course = courseDao.readCourseById(1L);

        Attendance attendanceGina = new Attendance();
        attendanceGina.setDate(LocalDate.of(2019, 11, 3));
        attendanceGina.setStudent(studentDao.readStudentByLastName("Agapescu"));
        attendanceGina.setPresent(true);
        attendanceGina.setCourse(course);
        attendanceGina.setTopic(topic);

        Attendance attendanceCorina = new Attendance();
        attendanceCorina.setDate(LocalDate.of(2019, 11, 3));
        attendanceCorina.setStudent(studentDao.readStudentByLastName("Costea"));
        attendanceCorina.setPresent(true);
        attendanceCorina.setTopic(topic);
        attendanceCorina.setCourse(course);

        Attendance attendanceCostin = new Attendance();
        attendanceCostin.setDate(LocalDate.of(2019, 11, 3));
        attendanceCostin.setStudent(studentDao.readStudentByLastName("Branzas"));
        attendanceCostin.setPresent(true);
        attendanceCostin.setTopic(topic);
        attendanceCostin.setCourse(course);

        Attendance attendanceIoana = new Attendance();
        attendanceIoana.setDate(LocalDate.of(2019, 11,3));
        attendanceIoana.setStudent(studentDao.readStudentByLastName("Secan"));
        attendanceIoana.setPresent(true);
        attendanceIoana.setTopic(topic);
        attendanceIoana.setCourse(course);

        Attendance attendanceRaul = new Attendance();
        attendanceRaul.setDate(LocalDate.of(2019, 11, 3));
        attendanceRaul.setStudent(studentDao.readStudentByLastName("Opris"));
        attendanceRaul.setPresent(true);
        attendanceRaul.setTopic(topic);
        attendanceRaul.setCourse(course);

        Attendance attendanceCipri = new Attendance();
        attendanceCipri.setDate(LocalDate.of(2019, 11, 3));
        attendanceCipri.setStudent(studentDao.readStudentByLastName("Cornea"));
        attendanceCipri.setPresent(true);
        attendanceCipri.setTopic(topic);
        attendanceCipri.setCourse(course);

        List<Attendance>attendanceList = new ArrayList<>();
        attendanceList.add(attendanceCipri);
        attendanceList.add(attendanceCorina);
        attendanceList.add(attendanceCostin);
        attendanceList.add(attendanceGina);
        attendanceList.add(attendanceIoana);
        attendanceList.add(attendanceRaul);

        for(Attendance attendance:attendanceList){
            attendanceDao.createAttendance(attendance);
        }

    }

    public static void saveAttendanceVarargs(){
        Topic topic = topicDao.readTopicById(8L);
        Course course = courseDao.readCourseById(1L);

        Attendance attendanceGina = new Attendance();
        attendanceGina.setDate(LocalDate.of(2019, 11, 4));
        attendanceGina.setStudent(studentDao.readStudentByLastName("Agapescu"));
        attendanceGina.setPresent(true);
        attendanceGina.setCourse(course);
        attendanceGina.setTopic(topic);

        Attendance attendanceCorina = new Attendance();
        attendanceCorina.setDate(LocalDate.of(2019, 11, 4));
        attendanceCorina.setStudent(studentDao.readStudentByLastName("Costea"));
        attendanceCorina.setPresent(true);
        attendanceCorina.setTopic(topic);
        attendanceCorina.setCourse(course);

        Attendance attendanceCostin = new Attendance();
        attendanceCostin.setDate(LocalDate.of(2019, 11, 4));
        attendanceCostin.setStudent(studentDao.readStudentByLastName("Branzas"));
        attendanceCostin.setPresent(true);
        attendanceCostin.setTopic(topic);
        attendanceCostin.setCourse(course);

        Attendance attendanceIoana = new Attendance();
        attendanceIoana.setDate(LocalDate.of(2019, 11,4));
        attendanceIoana.setStudent(studentDao.readStudentByLastName("Secan"));
        attendanceIoana.setPresent(true);
        attendanceIoana.setTopic(topic);
        attendanceIoana.setCourse(course);

        Attendance attendanceRaul = new Attendance();
        attendanceRaul.setDate(LocalDate.of(2019, 11, 4));
        attendanceRaul.setStudent(studentDao.readStudentByLastName("Opris"));
        attendanceRaul.setPresent(true);
        attendanceRaul.setTopic(topic);
        attendanceRaul.setCourse(course);

        Attendance attendanceCipri = new Attendance();
        attendanceCipri.setDate(LocalDate.of(2019, 11, 4));
        attendanceCipri.setStudent(studentDao.readStudentByLastName("Cornea"));
        attendanceCipri.setPresent(true);
        attendanceCipri.setTopic(topic);
        attendanceCipri.setCourse(course);

        List<Attendance>attendanceList = new ArrayList<>();
        attendanceList.add(attendanceCipri);
        attendanceList.add(attendanceCorina);
        attendanceList.add(attendanceCostin);
        attendanceList.add(attendanceGina);
        attendanceList.add(attendanceIoana);
        attendanceList.add(attendanceRaul);

        for(Attendance attendance:attendanceList){
            attendanceDao.createAttendance(attendance);
        }
    }

    public static void saveAttendanceCoding2(){
        Topic topic = topicDao.readTopicById(10L);
        Course course = courseDao.readCourseById(1L);

        Attendance attendanceGina = new Attendance();
        attendanceGina.setDate(LocalDate.of(2019, 11, 18));
        attendanceGina.setStudent(studentDao.readStudentByLastName("Agapescu"));
        attendanceGina.setPresent(true);
        attendanceGina.setCourse(course);
        attendanceGina.setTopic(topic);

        Attendance attendanceCorina = new Attendance();
        attendanceCorina.setDate(LocalDate.of(2019, 11, 18));
        attendanceCorina.setStudent(studentDao.readStudentByLastName("Costea"));
        attendanceCorina.setPresent(true);
        attendanceCorina.setTopic(topic);
        attendanceCorina.setCourse(course);

        Attendance attendanceCostin = new Attendance();
        attendanceCostin.setDate(LocalDate.of(2019, 11, 18));
        attendanceCostin.setStudent(studentDao.readStudentByLastName("Branzas"));
        attendanceCostin.setPresent(true);
        attendanceCostin.setTopic(topic);
        attendanceCostin.setCourse(course);

        Attendance attendanceIoana = new Attendance();
        attendanceIoana.setDate(LocalDate.of(2019, 11,18));
        attendanceIoana.setStudent(studentDao.readStudentByLastName("Secan"));
        attendanceIoana.setPresent(true);
        attendanceIoana.setTopic(topic);
        attendanceIoana.setCourse(course);

        Attendance attendanceRaul = new Attendance();
        attendanceRaul.setDate(LocalDate.of(2019, 11, 18));
        attendanceRaul.setStudent(studentDao.readStudentByLastName("Opris"));
        attendanceRaul.setPresent(true);
        attendanceRaul.setTopic(topic);
        attendanceRaul.setCourse(course);

        Attendance attendanceCipri = new Attendance();
        attendanceCipri.setDate(LocalDate.of(2019, 11, 18));
        attendanceCipri.setStudent(studentDao.readStudentByLastName("Cornea"));
        attendanceCipri.setPresent(true);
        attendanceCipri.setTopic(topic);
        attendanceCipri.setCourse(course);

        List<Attendance>attendanceList = new ArrayList<>();
        attendanceList.add(attendanceCipri);
        attendanceList.add(attendanceCorina);
        attendanceList.add(attendanceCostin);
        attendanceList.add(attendanceGina);
        attendanceList.add(attendanceIoana);
        attendanceList.add(attendanceRaul);

        for(Attendance attendance:attendanceList){
            attendanceDao.createAttendance(attendance);
        }
    }

    public static void saveAttendanceTesting(){
        Topic topic = topicDao.readTopicById(12L);
        Course course = courseDao.readCourseById(1L);

        Attendance attendanceGina = new Attendance();
        attendanceGina.setDate(LocalDate.of(2019, 11, 25));
        attendanceGina.setStudent(studentDao.readStudentByLastName("Agapescu"));
        attendanceGina.setPresent(true);
        attendanceGina.setCourse(course);
        attendanceGina.setTopic(topic);

        Attendance attendanceCorina = new Attendance();
        attendanceCorina.setDate(LocalDate.of(2019, 11, 25));
        attendanceCorina.setStudent(studentDao.readStudentByLastName("Costea"));
        attendanceCorina.setPresent(true);
        attendanceCorina.setTopic(topic);
        attendanceCorina.setCourse(course);

        Attendance attendanceCostin = new Attendance();
        attendanceCostin.setDate(LocalDate.of(2019, 11, 25));
        attendanceCostin.setStudent(studentDao.readStudentByLastName("Branzas"));
        attendanceCostin.setPresent(true);
        attendanceCostin.setTopic(topic);
        attendanceCostin.setCourse(course);

        Attendance attendanceIoana = new Attendance();
        attendanceIoana.setDate(LocalDate.of(2019, 11,25));
        attendanceIoana.setStudent(studentDao.readStudentByLastName("Secan"));
        attendanceIoana.setPresent(true);
        attendanceIoana.setTopic(topic);
        attendanceIoana.setCourse(course);

        Attendance attendanceRaul = new Attendance();
        attendanceRaul.setDate(LocalDate.of(2019, 11, 25));
        attendanceRaul.setStudent(studentDao.readStudentByLastName("Opris"));
        attendanceRaul.setPresent(true);
        attendanceRaul.setTopic(topic);
        attendanceRaul.setCourse(course);

        Attendance attendanceCipri = new Attendance();
        attendanceCipri.setDate(LocalDate.of(2019, 11, 25));
        attendanceCipri.setStudent(studentDao.readStudentByLastName("Cornea"));
        attendanceCipri.setPresent(true);
        attendanceCipri.setTopic(topic);
        attendanceCipri.setCourse(course);

        List<Attendance>attendanceList = new ArrayList<>();
        attendanceList.add(attendanceCipri);
        attendanceList.add(attendanceCorina);
        attendanceList.add(attendanceCostin);
        attendanceList.add(attendanceGina);
        attendanceList.add(attendanceIoana);
        attendanceList.add(attendanceRaul);

        for(Attendance attendance:attendanceList){
            attendanceDao.createAttendance(attendance);
        }
    }

    public static void saveAttendanceCoding3(){
        Topic topic = topicDao.readTopicById(11L);
        Course course = courseDao.readCourseById(1L);

        Attendance attendanceGina = new Attendance();
        attendanceGina.setDate(LocalDate.of(2019, 11, 24));
        attendanceGina.setStudent(studentDao.readStudentByLastName("Agapescu"));
        attendanceGina.setPresent(true);
        attendanceGina.setCourse(course);
        attendanceGina.setTopic(topic);

        Attendance attendanceCorina = new Attendance();
        attendanceCorina.setDate(LocalDate.of(2019, 11, 24));
        attendanceCorina.setStudent(studentDao.readStudentByLastName("Costea"));
        attendanceCorina.setPresent(true);
        attendanceCorina.setTopic(topic);
        attendanceCorina.setCourse(course);

        Attendance attendanceCostin = new Attendance();
        attendanceCostin.setDate(LocalDate.of(2019, 11, 24));
        attendanceCostin.setStudent(studentDao.readStudentByLastName("Branzas"));
        attendanceCostin.setPresent(true);
        attendanceCostin.setTopic(topic);
        attendanceCostin.setCourse(course);

        Attendance attendanceIoana = new Attendance();
        attendanceIoana.setDate(LocalDate.of(2019, 11,24));
        attendanceIoana.setStudent(studentDao.readStudentByLastName("Secan"));
        attendanceIoana.setPresent(true);
        attendanceIoana.setTopic(topic);
        attendanceIoana.setCourse(course);

        Attendance attendanceRaul = new Attendance();
        attendanceRaul.setDate(LocalDate.of(2019, 11, 24));
        attendanceRaul.setStudent(studentDao.readStudentByLastName("Opris"));
        attendanceRaul.setPresent(true);
        attendanceRaul.setTopic(topic);
        attendanceRaul.setCourse(course);

        Attendance attendanceCipri = new Attendance();
        attendanceCipri.setDate(LocalDate.of(2019, 11, 24));
        attendanceCipri.setStudent(studentDao.readStudentByLastName("Cornea"));
        attendanceCipri.setPresent(true);
        attendanceCipri.setTopic(topic);
        attendanceCipri.setCourse(course);

        List<Attendance>attendanceList = new ArrayList<>();
        attendanceList.add(attendanceCipri);
        attendanceList.add(attendanceCorina);
        attendanceList.add(attendanceCostin);
        attendanceList.add(attendanceGina);
        attendanceList.add(attendanceIoana);
        attendanceList.add(attendanceRaul);

        for(Attendance attendance:attendanceList){
            attendanceDao.createAttendance(attendance);
        }
    }

    public static void saveAttendanceCoding1(){
        Topic topic = topicDao.readTopicById(9L);
        Course course = courseDao.readCourseById(1L);

        Attendance attendanceGina = new Attendance();
        attendanceGina.setDate(LocalDate.of(2019, 11, 17));
        attendanceGina.setStudent(studentDao.readStudentByLastName("Agapescu"));
        attendanceGina.setPresent(true);
        attendanceGina.setCourse(course);
        attendanceGina.setTopic(topic);

        Attendance attendanceCorina = new Attendance();
        attendanceCorina.setDate(LocalDate.of(2019, 11, 17));
        attendanceCorina.setStudent(studentDao.readStudentByLastName("Costea"));
        attendanceCorina.setPresent(true);
        attendanceCorina.setTopic(topic);
        attendanceCorina.setCourse(course);

        Attendance attendanceCostin = new Attendance();
        attendanceCostin.setDate(LocalDate.of(2019, 11, 17));
        attendanceCostin.setStudent(studentDao.readStudentByLastName("Branzas"));
        attendanceCostin.setPresent(true);
        attendanceCostin.setTopic(topic);
        attendanceCostin.setCourse(course);

        Attendance attendanceIoana = new Attendance();
        attendanceIoana.setDate(LocalDate.of(2019, 11,17));
        attendanceIoana.setStudent(studentDao.readStudentByLastName("Secan"));
        attendanceIoana.setPresent(true);
        attendanceIoana.setTopic(topic);
        attendanceIoana.setCourse(course);

        Attendance attendanceRaul = new Attendance();
        attendanceRaul.setDate(LocalDate.of(2019, 11, 18));
        attendanceRaul.setStudent(studentDao.readStudentByLastName("Opris"));
        attendanceRaul.setPresent(true);
        attendanceRaul.setTopic(topic);
        attendanceRaul.setCourse(course);

        Attendance attendanceCipri = new Attendance();
        attendanceCipri.setDate(LocalDate.of(2019, 11, 17));
        attendanceCipri.setStudent(studentDao.readStudentByLastName("Cornea"));
        attendanceCipri.setPresent(true);
        attendanceCipri.setTopic(topic);
        attendanceCipri.setCourse(course);

        List<Attendance>attendanceList = new ArrayList<>();
        attendanceList.add(attendanceCipri);
        attendanceList.add(attendanceCorina);
        attendanceList.add(attendanceCostin);
        attendanceList.add(attendanceGina);
        attendanceList.add(attendanceIoana);
        attendanceList.add(attendanceRaul);

        for(Attendance attendance:attendanceList){
            attendanceDao.createAttendance(attendance);
        }
    }


    public static Set<Topic>saveTopic(Module module){
        Set<Topic>topicSet = new HashSet<>();

        Topic history = new Topic();
        history.setTopicName("History of java, About java, First application");
        history.setDate(LocalDate.of(2019, 10, 12));
        history.setModule(module);
        topicSet.add(history);

        Topic variableTypes = new Topic();
        variableTypes.setTopicName("Data types, Variable");
        variableTypes.setDate(LocalDate.of(2019, 10,13) );
        variableTypes.setModule(module);
        topicSet.add(variableTypes);

        Topic operators = new Topic();
        operators.setTopicName("Operators and Casts");
        operators.setDate(LocalDate.of(2019, 10, 19));
        operators.setModule(module);
        topicSet.add(operators);

        Topic stringTopic = new Topic();
        stringTopic.setTopicName("Strings");
        stringTopic.setDate(LocalDate.of(2019, 10, 20));
        stringTopic.setModule(module);
        topicSet.add(stringTopic);

        Topic controlFlow = new Topic();
        controlFlow.setTopicName("Control flow, Loops");
        controlFlow.setDate(LocalDate.of(2019, 10, 26));
        controlFlow.setModule(module);
        topicSet.add(controlFlow);

        Topic arrayTopic = new Topic();
        arrayTopic.setTopicName("Arrays");
        arrayTopic.setDate(LocalDate.of(2019, 10, 27));
        arrayTopic.setModule(module);
        topicSet.add(arrayTopic);

        Topic oop = new Topic();
        oop.setTopicName("Object oriented programming");
        oop.setDate(LocalDate.of(2019, 11, 2));
        oop.setModule(module);
        topicSet.add(oop);

        Topic varargTopic = new Topic();
        varargTopic.setTopicName("Varargs, Date and time");
        varargTopic.setDate(LocalDate.of(2019, 11, 3));
        varargTopic.setModule(module);
        topicSet.add(varargTopic);

        Topic coding1 = new Topic();
        coding1.setTopicName("Java fundamentals-Coding");
        coding1.setDate(LocalDate.of(2019, 11, 16));
        coding1.setModule(module);
        topicSet.add(coding1);

        Topic codingSecond = new Topic();
        codingSecond.setTopicName("Java fundamentals-Coding-Part2");
        codingSecond.setDate(LocalDate.of(2019, 11, 17));
        codingSecond.setModule(module);
        topicSet.add(codingSecond);

        Topic coding3 = new Topic();
        coding3.setTopicName("Java fundamentals-Coding-Part3");
        coding3.setDate(LocalDate.of(2019, 11, 23));
        coding3.setModule(module);
        topicSet.add(coding3);

        Topic testing = new Topic();
        testing.setTopicName("Software Testing-fundamentals");
        testing.setDate(LocalDate.of(2019, 11, 24));
        testing.setModule(module);
        topicSet.add(testing);

        return topicSet;
    }

    public static void saveMaintainer(){
        Maintainer maintainer = new Maintainer();
        maintainer.setUserName("Mihai");
        maintainer.setFirstName("Mihai");
        maintainer.setLastName("Puscas");
        maintainer.setPassword("1234");
        maintainer.setStatus(Status.MAINTAINER);
        maintainer.setEmail("mihai.puscas@gmail.com");
        maintainer.setPhone("0745321654");
        maintainerDao.createMaintainer(maintainer);
    }

    public static void displayInfoAttendance(){
        System.out.println("-------------INFO ATTENDANCE--------------");
        for(Attendance attendance: attendanceDao.readAllAttendance()){
            attendance.displayInfoAttendance();
            System.out.println();
        }
        System.out.println("-------------------------------------");
    }

    public static void displayInfoMaintainer(){
        System.out.println("----------------------");
        for(Maintainer maintainer :maintainerDao.readAllMaintainer()){
            maintainer.displayInfoMaintainer();
            System.out.println();
        }
        System.out.println("----------------------");
    }

    public static Set<Student> saveStudent(Set<Course>courseSet){
        Student corina = new Student();
        Set<Student>studentSet = new HashSet<>();
        corina.setFirstName("Corina");
        corina.setLastName("Costea");
        corina.setUserName("Corina");
        corina.setStudentCnp("2854564254");
        corina.setGender(Gender.FEMALE);
        corina.setEmail("corina.costea@gamil.com");
        corina.setPassword("corina");
        corina.setProfession("business in commerce");
        corina.setPhone("0721321456");
        corina.setStatus(Status.STUDENT);
        corina.setDateOfBirth(LocalDate.of(1973, 8, 3));
        corina.setCourseSet(courseSet);
        studentSet.add(corina);

        Student costin = new Student();
        costin.setFirstName("Costin");
        costin.setLastName("Branzas");
        costin.setUserName("Costin");
        costin.setStudentCnp("123456445");
        costin.setEmail("costin.branzas@gmail.com");
        costin.setPhone("0721321789");
        costin.setStatus(Status.STUDENT);
        costin.setGender(Gender.MALE);
        costin.setPassword("costin");
        costin.setProfession("engineer");
        costin.setDateOfBirth(LocalDate.of(1985, 10, 10));
        costin.setCourseSet(courseSet);
        studentSet.add(costin);

        Student gina = new Student();
        gina.setFirstName("Gina");
        gina.setLastName("Agapescu");
        gina.setUserName("Gina");
        gina.setStudentCnp("2670228054708");
        gina.setProfession("advertising decorator");
        gina.setPassword("gina");
        gina.setPhone("0745985264");
        gina.setEmail("gina.agapescu@gmail.com");
        gina.setDateOfBirth(LocalDate.of(1967, 2, 28));
        gina.setGender(Gender.FEMALE);
        gina.setStatus(Status.STUDENT);
        gina.setCourseSet(courseSet);
        studentSet.add(gina);

        Student cipri = new Student();
        cipri.setFirstName("Ciprian");
        cipri.setLastName("Cornea");
        cipri.setUserName("Ciprian");
        cipri.setStudentCnp("1522454212");
        cipri.setPhone("0745321654");
        cipri.setPassword("raul");
        cipri.setEmail("ciprian.cornea@gmail.com");
        cipri.setDateOfBirth(LocalDate.of(1990, 10, 10));
        cipri.setStatus(Status.STUDENT);
        cipri.setGender(Gender.MALE);
        cipri.setProfession("freelance");
        cipri.setCourseSet(courseSet);
        studentSet.add(cipri);

        Student ioana = new Student();
        ioana.setFirstName("Ioana");
        ioana.setLastName("Secan");
        ioana.setUserName("Ioana");
        ioana.setStudentCnp("28512100584215");
        ioana.setPhone("0721987456");
        ioana.setPassword("ioana");
        ioana.setEmail("ioana.secan@gmail.com");
        ioana.setDateOfBirth(LocalDate.of(1980, 7, 12));
        ioana.setStatus(Status.STUDENT);
        ioana.setGender(Gender.FEMALE);
        ioana.setProfession("sales agent");
        ioana.setCourseSet(courseSet);
        studentSet.add(ioana);

        Student raul = new Student();
        raul.setFirstName("Raul");
        raul.setLastName("Opris");
        raul.setUserName("Raul");
        raul.setStudentCnp("13254564854");
        raul.setPhone("0721213654");
        raul.setPassword("raul");
        raul.setEmail("raul.opris@gamail.com");
        raul.setDateOfBirth(LocalDate.of(1981, 10, 12));
        raul.setStatus(Status.STUDENT);
        raul.setGender(Gender.MALE);
        raul.setProfession("real estate agent");
        raul.setCourseSet(courseSet);
        studentSet.add(raul);

    return studentSet;

    }

    public static Trainer saveTrainer(Module module){
        Trainer trainer = new Trainer();
        trainer.setFirstName("Alina");
        trainer.setLastName("Noge");
        trainer.setUserName("Alina");
        trainer.setEmail("alina.noge@gmail.com");
        trainer.setPassword("alina");
        trainer.setPhone("0745147852");
        trainer.setStatus(Status.TRAINER);
        trainer.setCertificate(true);
        trainer.setModule(module);

        return trainer;
    }

    public static Trainer saveTrainer1(Module module){
        Trainer trainer = new Trainer();
        trainer.setFirstName("Csabi");
        trainer.setLastName("Biro");
        trainer.setEmail("csabi.biro@gmail.com");
        trainer.setPassword("csabi");
        trainer.setPhone("0745369852");
        trainer.setStatus(Status.TRAINER);
        trainer.setCertificate(true);
        trainer.setModule(module);

        return trainer;
    }

    public static Set<Module> saveModule(Course course ){
        Module module = new Module();
        Set<Module>moduleSet = new HashSet<>();
        Set<Topic>topicSet = saveTopic(module);
        module.setModuleName("Java-Fundamentals");
        module.setTrainer(saveTrainer(module));
        module.setDeploymentMode(DeploymentMode.CLASSROOM);
        module.setTopicSet(topicSet);
        module.setCourse(course);
        moduleSet.add(module);

       // Module module1 = new Module();
       // module1.setModuleName("Java-Advance");
       // module1.setTrainer(saveTrainer1(module));
       // module1.setCourse(course);
       // module1.setStudentSet(saveStudent());
       // moduleSet.add(module1);

        //moduleDao.createModule(module);
        return moduleSet;

    }

    public static void saveCourse(){
        Course course = new Course();
        Set<Course>courseSet = new HashSet<>();
        Set<Student>studentSet = saveStudent(courseSet);
        Set<Module>moduleSet = saveModule(course);
        course.setCourseName("Java2-Oradea");
        course.setBeginDate(LocalDate.of(2019, 10, 12));
        course.setEndingDate(LocalDate.of(2020, 6, 21));
        course.setModuleSet(moduleSet);
        course.setStudentSet(studentSet);
        courseSet.add(course);
        courseDao.createCourse(course);
    }

    public static void displayInfoTopic(){
        System.out.println("------------INFO TOPIC------------- ");
        for(Topic topic: topicDao.readAllTopic()){
            topic.displayInfoTopics();
            System.out.println();
        }
        System.out.println("----------------------------");
    }

    public static void displayInfoStudent(){
        System.out.println("-----------INFO STUDENT----------");
        for(Student student:studentDao.readAllStudent()){
            student.displayInfoStudent();
            System.out.println();
        }
        System.out.println("----------------------------------");
    }

    public static void displayInfoTrainer(){
        System.out.println("-------INFO TRAINER--------");
        for(Trainer trainer:trainerDao.readAllTrainer()){
            trainer.displayInfoTrainer();
            System.out.println();
        }
    }

    public static void displayInfoModule(){
        System.out.println("------------INFO MODULE-------------");
        for(Module module:moduleDao.readAllModule()){
            module.displayInfoModule();
            System.out.println();
        }
        System.out.println("-------------------------------");
    }

    public static void displayInfoCourse(){
        System.out.println("------------INFO COURSE------------");
        for(Course course:courseDao.readAllCourse()){
            course.displayInfoCourse();
            System.out.println();
        }
        System.out.println("--------------------------------------");
    }
}
