package com.sda.Gina.sudentRegister.view;

import com.sda.Gina.sudentRegister.model.*;
import com.sda.Gina.sudentRegister.repository.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.net.URL;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;

public class CourseDataController implements Initializable {
    static StudentDao studentDao = new StudentHibernate();
    static CourseDao courseDao = new CourseHibernate();
    static ModuleDao moduleDao = new ModuleHibernate();
    static TopicDao topicDao = new TopicHibernate();
    static TrainerDao trainerDao = new TrainerHibernate();

    @FXML private Label studentIdLbl;
    @FXML private Label courseIdLbl;
    @FXML private Label moduleIdLbl;
    @FXML private Label topicIdLbl;
    @FXML private Label trainerIdLbl;

    @FXML private TextField studentFirstNameTxt;
    @FXML private TextField studentLastNameTxt;
    @FXML private PasswordField studentPassword;
    @FXML private TextField studentPhoneTxt;
    @FXML private TextField studentEmailTxt;
    @FXML private ComboBox<Gender>genderCmb;
    @FXML private DatePicker datePickerDateOfBirth;
    @FXML private TextField professionTxt;
    @FXML private ComboBox<Status>studentStatusCmb;
    @FXML private ComboBox<String>studentFirstNameCmb;
    @FXML private ComboBox<String>studentLastNameCmb;

    @FXML private ComboBox<String>trainerFirstNameCmb;
    @FXML private ComboBox<String>trainerLastNameCmb;
    @FXML private TextField trainerFirstNameTxt;
    @FXML private TextField trainerLastNameTxt;
    @FXML private PasswordField trainerPassword;
    @FXML private TextField trainerPhoneTxt;
    @FXML private TextField trainerEmailTxt;
    @FXML private ComboBox<Boolean>certificateCmb;
    @FXML private ComboBox<Status>trainerStatusCmb;

    @FXML private ComboBox<DeploymentMode>deploymentModeCmb;
    @FXML private ComboBox<String>moduleCmb;
    @FXML private TextField moduleNameTxt;

    @FXML private ComboBox<String>topicCmb;
    @FXML private TextField  topicNameTxt;
    @FXML private DatePicker topicDate;

    @FXML private ComboBox<String>courseCmb;
    @FXML private TextField courseNameTxt;
    @FXML private DatePicker beginDatePicker;
    @FXML private  DatePicker endDatePicker;

    @FXML private ObservableList<Topic> dataTopic = FXCollections.observableArrayList();
    @FXML private TableView<Topic>topicTable;
    @FXML private TableColumn<Topic, String>topicNameCol;
    @FXML private TableColumn<Topic, LocalDate>topicDateCol;

   /* @FXML private ObservableList<Module>dataModule = FXCollections.observableArrayList();
    @FXML private TableView<Module>moduleTable;
    @FXML private TableColumn<Module, String>moduleNameCol;
    @FXML private TableColumn<Module, Trainer>moduleTrainerCol;
    @FXML private TableColumn<Module, DeploymentMode>deploymentModeTableCol;*/

    @FXML private ObservableList<Student>dataStudent = FXCollections.observableArrayList();
    @FXML private TableView<Student>studentTable;
    @FXML private TableColumn<Student, String>studentLastNameCol;
    @FXML private TableColumn<Student, String>studentFirstNameCol;
    @FXML private TableColumn<Student, String>studentPhoneCol;
    @FXML private TableColumn<Student, String>studentEmailCol;
    @FXML private TableColumn<Student, String>studentPasswordCol;
    @FXML private TableColumn<Student, Enum<Gender>>studentGenderCol;
    @FXML private TableColumn<Student, String>studentProfessionCol;
    @FXML private TableColumn<Student, LocalDate>studentDateOfBirthCol;
    @FXML private TableColumn<Student, Enum<Status>>studentStatusCol;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fillComboBoxCourse();
        genderCmb.getItems().setAll(Gender.FEMALE, Gender.MALE);
        deploymentModeCmb.getItems().setAll(DeploymentMode.CLASSROOM, DeploymentMode.ONLINE);
        studentStatusCmb.getItems().setAll(Status.STUDENT);
        certificateCmb.getItems().addAll(true, false);
        trainerStatusCmb.getItems().setAll(Status.TRAINER);

        //STUDENT TABLE
        studentTable.setEditable(true);
        studentFirstNameCol.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        studentLastNameCol.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        studentPasswordCol.setCellValueFactory(new PropertyValueFactory<>("password"));
        studentPhoneCol.setCellValueFactory(new PropertyValueFactory<>("phone"));
        studentEmailCol.setCellValueFactory(new PropertyValueFactory<>("email"));
        studentDateOfBirthCol.setCellValueFactory(new PropertyValueFactory<>("date"));
        studentGenderCol.setCellValueFactory(new PropertyValueFactory<>("gender"));
        studentProfessionCol.setCellValueFactory(new PropertyValueFactory<>("profession"));
        studentStatusCol.setCellValueFactory(new PropertyValueFactory<>("status"));
        studentTable.setItems(dataStudent);

        //TOPIC TABLE
        topicTable.setEditable(true);
        topicNameCol.setCellValueFactory(new PropertyValueFactory<>("topicName"));
        topicDateCol.setCellValueFactory(new PropertyValueFactory<>("date"));
        topicTable.setItems(dataTopic);
    }

    //SAVE TRAINER
    @FXML
    private void saveTrainer(){
        Trainer trainer = new Trainer();
        trainer.setModule(moduleDao.readModuleById(Long.parseLong(moduleIdLbl.getText())));
        trainer.setCertificate(certificateCmb.getValue());
        trainer.setStatus(trainerStatusCmb.getValue());
        trainer.setPhone(trainerPhoneTxt.getText());
        trainer.setPassword(trainerPassword.getText());
        trainer.setEmail(trainerEmailTxt.getText());
        trainer.setLastName(trainerLastNameTxt.getText());
        trainer.setFirstName(trainerFirstNameTxt.getText());
        trainerDao.createTrainer(trainer);

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Trainer successfully saved!");
        alert.showAndWait();
        displayInfoTrainer();
    }

    //CLEAR TEXT FIELD MODULE
    private void clearTextFieldsModule(){
        deploymentModeCmb.getSelectionModel().clearSelection();
        moduleNameTxt.setText("");
    }

    //CLEAR TEXT FIELD TOPIC
    private void clearTextFieldsTopic(){
        topicNameTxt.setText("");
        topicDate.setValue(LocalDate.now());
    }

    //CLEAR TEXT FIELD TRAINER
    private void clearTextFieldsTrainer(){
        trainerPhoneTxt.setText("");
        trainerFirstNameTxt.setText("");
        trainerLastNameTxt.setText("");
        trainerStatusCmb.getSelectionModel().clearSelection();
        trainerPassword.setText("");
        trainerEmailTxt.setText("");
        certificateCmb.getSelectionModel().clearSelection();
    }

    @FXML
    private void deleteTopic(){
        topicDao.deleteTopic(Long.parseLong(topicIdLbl.getText()));
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Topic successfully deleted!");
        alert.showAndWait();
        displayInfoTopic();
    }

    @FXML
    private void deleteModule(){
        moduleDao.deleteModule(Long.parseLong(moduleIdLbl.getText()));
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Module successfully deleted!");
        alert.showAndWait();
        clearTextFieldsStudent();
        displayInfoTopic();
        displayInfoModule();
    }

    @FXML
    private void saveModuleWithTrainerAndTopics(){
        Module module = new Module();
        module.setModuleName(moduleNameTxt.getText());
        module.setDeploymentMode(deploymentModeCmb.getValue());
        module.setTrainer(saveTrainerForModule(module));
        module.setCourse(courseDao.readCourseById(Long.parseLong(courseIdLbl.getText())));
        Set<Topic>topicSet = new HashSet<>();
        for(Topic topic : topicTable.getItems()){
            topic.setModule(module);
            topicSet.add(topic);
        }
        module.setTopicSet(topicSet);
        moduleDao.createModule(module);
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Module successfully saved!");
        alert.showAndWait();
        clearTextFieldsTopic();
        clearTextFieldsModule();
        clearTextFieldsTrainer();
        topicTable.getItems().clear();
        topicTable.refresh();
        displayInfoModule();
        displayInfoTopic();
    }

    //CREATE TOPICS FOR TABLE
    @FXML
    private void createTopicsForTable(){
        Topic topic = new Topic();
        topic.setTopicName(topicNameTxt.getText());
        topic.setDate(topicDate.getValue());
        dataTopic.add(topic);

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation ?");
        String s = "Do you want to save this topic ?";
        alert.setContentText(s);
        Optional<ButtonType> result = alert.showAndWait();

        if ((result.isPresent()) && (result.get() == ButtonType.OK) ) {

            saveModuleWithTrainerAndTopics();

        }
        clearTextFieldsTopic();
    }

    //CREATE TRAINER FOR MODULE
    private Trainer saveTrainerForModule(Module module){
        Trainer trainer = new Trainer();
        trainer.setModule(module);
        trainer.setCertificate(certificateCmb.getValue());
        trainer.setStatus(trainerStatusCmb.getValue());
        trainer.setPhone(trainerPhoneTxt.getText());
        trainer.setPassword(trainerPassword.getText());
        trainer.setEmail(trainerEmailTxt.getText());
        trainer.setLastName(trainerLastNameTxt.getText());
        trainer.setFirstName(trainerFirstNameTxt.getText());

        return trainer;
    }

    //DELETE STUDENT
    @FXML
    private void deleteStudent(){
        studentDao.deleteStudent(Long.parseLong(studentIdLbl.getText()));

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Student successfully deleted!");
        alert.showAndWait();
        clearTextFieldsStudent();
        displayInfoStudent();
    }

    //SEARCH STUDENT BY FIRST NAME AND LAST NAME
    @FXML
    private Student searchStudentByFirstAndLastName(){
        Student student = studentDao.readStudentByFirstNameAndLastName(studentFirstNameCmb.getValue(),studentLastNameCmb.getValue());
        studentIdLbl.setText(student.getId().toString());
        studentFirstNameTxt.setText(studentFirstNameCmb.getValue());
        studentLastNameTxt.setText(studentLastNameCmb.getValue());
        studentPassword.setText(student.getPassword());
        studentPhoneTxt.setText(student.getPhone());
        studentEmailTxt.setText(student.getEmail());
        professionTxt.setText(student.getProfession());
        datePickerDateOfBirth.setValue(student.getDateOfBirth());
        genderCmb.setValue(student.getGender());
        studentStatusCmb.setValue(student.getStatus());

        return student;
    }
    //UPDATE STUDENT
    @FXML
    private void updateStudent(){
        Student student = studentDao.readStudentById(Long.parseLong(studentIdLbl.getText()));
        student.setDateOfBirth(datePickerDateOfBirth.getValue());
        student.setStatus(studentStatusCmb.getValue());
        student.setGender(genderCmb.getValue());
        student.setProfession(professionTxt.getText());
        student.setEmail(studentEmailTxt.getText());
        student.setPhone(studentPhoneTxt.getText());
        student.setPassword(studentPassword.getText());
        student.setLastName(studentLastNameTxt.getText());
        student.setFirstName(studentFirstNameTxt.getText());
        student.setCourseSet(searchCourse());

        studentDao.updateStudent(student);

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Student successfully updated!");
        alert.showAndWait();
        clearTextFieldsStudent();
        displayInfoStudent();
    }

    //SEARCH COURSE
    private Set<Course>searchCourse(){
        Course course = courseDao.readCourseById(Long.parseLong(courseIdLbl.getText()));
        Set<Course>courseSet = new HashSet<>();
        courseSet.add(course);
        return courseSet;
    }

    //SET COURSE
    private Set<Course> searchCourseForTable(Set<Student>studentSet){
        Course course = courseDao.readCourseById(Long.parseLong(courseIdLbl.getText()));
        Set<Course>courseSet = new HashSet<>();
        courseSet.add(course);
        return courseSet;
    }

    //SAVE STUDENT
    @FXML
    private void saveStudent(){
        Set<Student>studentSet = new HashSet<>();
        for(Student student:studentTable.getItems()){
            studentSet.add(student);
            student.setCourseSet(searchCourseForTable(studentSet));
            studentDao.createStudent(student);
        }
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Student successfully saved!");
        alert.showAndWait();

        studentTable.getItems().clear();
        studentTable.refresh();
        displayInfoStudent();

    }

    // CLEAR TEXT FIELDS STUDENT
    private void clearTextFieldsStudent(){
        studentFirstNameTxt.setText("");
        studentLastNameTxt.setText("");
        genderCmb.getSelectionModel().clearSelection();
        studentStatusCmb.getSelectionModel().clearSelection();
        studentPassword.setText("");
        studentPhoneTxt.setText("");
        studentEmailTxt.setText("");
        professionTxt.setText("");
        datePickerDateOfBirth.setValue(LocalDate.now());
    }


    //ADD STUDENT TO TABLE
    @FXML
    private void addStudentToTable(){
        Student student = new Student();
        student.setFirstName(studentFirstNameTxt.getText());
        student.setLastName(studentLastNameTxt.getText());
        student.setPassword(studentPassword.getText());
        student.setPhone(studentPhoneTxt.getText());
        student.setEmail(studentEmailTxt.getText());
        student.setProfession(professionTxt.getText());
        student.setGender(genderCmb.getValue());
        student.setStatus(studentStatusCmb.getValue());
        student.setDateOfBirth(datePickerDateOfBirth.getValue());
        dataStudent.add(student);

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation ?");
        String s = "Do you want to save this student ?";
        alert.setContentText(s);
        Optional<ButtonType> result = alert.showAndWait();

        if ((result.isPresent()) && (result.get() == ButtonType.OK) ) {

            saveStudent();
        }
        clearTextFieldsStudent();
    }


    @FXML
    private void handleTrainerPhone(KeyEvent ke){
        if (ke.getCode().equals(KeyCode.ENTER)) {
            trainerEmailTxt.requestFocus();
        }
    }

    @FXML
    private void handleTrainerPassword(KeyEvent ke){
        if (ke.getCode().equals(KeyCode.ENTER)) {
            trainerPhoneTxt.requestFocus();
        }
    }

    @FXML
    private void handleTrainerLastName(KeyEvent ke){
        if (ke.getCode().equals(KeyCode.ENTER)) {
            trainerStatusCmb.requestFocus();
        }
    }

    @FXML
    private void handleTrainerFirstName(KeyEvent ke){
        if (ke.getCode().equals(KeyCode.ENTER)) {
            trainerLastNameTxt.requestFocus();
        }
    }

    @FXML
    private void retrieveTrainerInfo(){
        Trainer trainer = trainerDao.readTrainerByFirstAndLastName(
                trainerFirstNameCmb.getValue(), trainerLastNameCmb.getValue());
        trainerIdLbl.setText(trainer.getId().toString());
        trainerFirstNameTxt.setText(trainerFirstNameCmb.getValue());
        trainerLastNameTxt.setText(trainerLastNameCmb.getValue());
        trainerPassword.setText(trainer.getPassword());
        trainerPhoneTxt.setText(trainer.getPhone());
        trainerEmailTxt.setText(trainer.getEmail());
        trainerStatusCmb.setValue(trainer.getStatus());
        certificateCmb.setValue(trainer.isCertificate());
    }

    @FXML
    private void bindTrainersByModule(){
       fillComboBoxTrainerFirstName();
       fillComboBoxTrainerLastName();

    }

    private void fillComboBoxTrainerLastName(){
        ObservableList<String>trainerLastNames = FXCollections.observableArrayList(
                moduleDao.readTrainerLastName(Long.parseLong(moduleIdLbl.getText())));
        trainerLastNameCmb.setItems(trainerLastNames);
    }

    private void fillComboBoxTrainerFirstName(){
        ObservableList<String>trainerFirstNames = FXCollections.observableArrayList(
                moduleDao.readTrainerFirstName(Long.parseLong(moduleIdLbl.getText())));
        trainerFirstNameCmb.setItems(trainerFirstNames);
    }

    @FXML
    private void retrieveStudentInfo(){
        Student student = studentDao.readStudentByFirstNameAndLastName(studentFirstNameCmb.getValue(),studentLastNameCmb.getValue());
        studentIdLbl.setText(student.getId().toString());
        studentFirstNameTxt.setText(studentFirstNameCmb.getValue());
        studentLastNameTxt.setText(studentLastNameCmb.getValue());
        studentPassword.setText(student.getPassword());
        studentPhoneTxt.setText(student.getPhone());
        studentEmailTxt.setText(student.getEmail());
        professionTxt.setText(student.getProfession());
        datePickerDateOfBirth.setValue(student.getDateOfBirth());
        genderCmb.setValue(student.getGender());
        studentStatusCmb.setValue(student.getStatus());

    }

    @FXML
    private void bindStudentsByCourse(){
        fillComboBoxLastNameStudent();
        fillComboBoxFirstNameStudent();
    }

    private void fillComboBoxLastNameStudent(){
        ObservableList<String>studentLastNames = FXCollections.observableArrayList
                (studentDao.readStudentLastNameForComboBox(Long.parseLong(courseIdLbl.getText())));
        studentLastNameCmb.setItems(studentLastNames);

    }

    private void fillComboBoxFirstNameStudent(){
        ObservableList<String>studentFirstNames = FXCollections.observableArrayList
                (studentDao.readStudentFirstNameForComboBox(Long.parseLong(courseIdLbl.getText())));
         studentFirstNameCmb.setItems(studentFirstNames);

    }

    @FXML
    private void handlesStudentFirstName(KeyEvent ke){
        if (ke.getCode().equals(KeyCode.ENTER)) {
            studentLastNameTxt.requestFocus();
        }
    }

    @FXML
    private void handlesStudentLastName(KeyEvent ke){
        if (ke.getCode().equals(KeyCode.ENTER)) {
            studentStatusCmb.requestFocus();

        }
    }

    @FXML
    private void handlesStudentPassword(KeyEvent ke){
        if (ke.getCode().equals(KeyCode.ENTER)) {
            studentEmailTxt.requestFocus();

        }
    }

    @FXML
    private void handlesStudentPhone(KeyEvent ke){
        if (ke.getCode().equals(KeyCode.ENTER)) {
            professionTxt.requestFocus();

        }
    }

    @FXML
    private void handlesStudentEmail(KeyEvent ke){
        if (ke.getCode().equals(KeyCode.ENTER)) {
            studentPhoneTxt.requestFocus();

        }
    }

    @FXML
    private void retrieveTopicInfo(){
        Topic topic = topicDao.readTopicByName(topicCmb.getValue());
        topicIdLbl.setText(String.valueOf(topic.getTopicId()));
        topicDate.setValue(topic.getDate());
        topicNameTxt.setText(topicCmb.getValue());
    }

    @FXML
    private void fillComboBoxTopicByModule(){
        ObservableList<String>topicNames = FXCollections.observableArrayList
                (topicDao.readTopicToBindModule(Long.parseLong(moduleIdLbl.getText())));
        topicCmb.setItems(topicNames);
    }

    @FXML
    private void retrieveModuleInfo(){
        Module module = moduleDao.readModuleByName(moduleCmb.getValue());

        moduleIdLbl.setText(String.valueOf(module.getModuleId()));
        moduleNameTxt.setText(moduleCmb.getValue());
        deploymentModeCmb.setValue(module.getDeploymentMode());

        Trainer trainer = moduleDao.findTrainerByModule(module.getModuleId());
        trainerFirstNameTxt.setText(trainer.getFirstName());
        trainerIdLbl.setText(trainer.getId().toString());
        trainerLastNameTxt.setText(trainer.getLastName());
        trainerPassword.setText(trainer.getPassword());
        trainerPhoneTxt.setText(trainer.getPhone());
        trainerStatusCmb.setValue(trainer.getStatus());
        certificateCmb.setValue(trainer.isCertificate());
    }

    @FXML
    public void fillComboBoxModuleByCourse(){
        ObservableList<String>moduleNames = FXCollections.observableArrayList
                (moduleDao.readModuleToBindCourse(Long.parseLong(courseIdLbl.getText())));
        moduleCmb.setItems(moduleNames);

    }

    @FXML
    private void retrieveCourseInfo(){
        Course course = courseDao.readCourseByName(courseCmb.getValue());
        courseNameTxt.setText(courseCmb.getValue());
        courseIdLbl.setText(String.valueOf(course.getCourseId()));
        beginDatePicker.setValue(course.getBeginDate());
        endDatePicker.setValue(course.getEndingDate());
    }

    private void fillComboBoxCourse(){
        ObservableList<String>courseNames = FXCollections.observableArrayList(courseDao.readCourseNames());
        courseCmb.setItems(courseNames);
    }

    //SAVE TOPIC
    @FXML
    private void saveTopic(){
        for(Topic topic : topicTable.getItems()){
            topic.setModule(moduleDao.readModuleById(Long.parseLong(moduleIdLbl.getText())));
            topicDao.createTopic(topic);
        }

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Module successfully saved!");
        alert.showAndWait();
        displayInfoTopic();
    }

    //SAVE MODULE
    @FXML
    private void saveModule(){
        Module module = new Module();
        module.setModuleName(moduleNameTxt.getText());
        module.setDeploymentMode(deploymentModeCmb.getValue());
        module.setTrainer(saveTrainerForModule(module));
        module.setCourse(courseDao.readCourseById(Long.parseLong(courseIdLbl.getText())));

        moduleDao.createModule(module);
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Module successfully saved!");
        alert.showAndWait();
        displayInfoModule();
    }

    //SAVE COURSE
    @FXML
    private void saveCourse(){
        Course course = new Course();
        Set<Course>courseSet = new HashSet<>();
        course.setCourseName(courseNameTxt.getText());
        course.setBeginDate(beginDatePicker.getValue());
        course.setEndingDate(endDatePicker.getValue());
        Set<Student>studentSet = new HashSet<>();
        for(Student student : studentTable.getItems()){
            courseSet.add(course);
            studentSet.add(student);
            student.setCourseSet(courseSet);
            course.setStudentSet(studentSet);
        }
        courseDao.createCourse(course);
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Course successfully saved!");
        alert.showAndWait();

        displayInfoCourse();

    }


    //UPDATE MODULE
    @FXML
    private void updateModule(){
        Module module = moduleDao.readModuleById(Long.parseLong(moduleIdLbl.getText()));
        module.setModuleName(moduleNameTxt.getText());
        module.setDeploymentMode(deploymentModeCmb.getValue());
        module.setCourse(courseDao.readCourseById(Long.parseLong(courseIdLbl.getText())));

        moduleDao.updateModule(module);
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Module successfully updated!");
        alert.showAndWait();
        displayInfoModule();
    }

    //UPDATE TOPIC
    @FXML
    private void updateTopic(){
        Topic topic = topicDao.readTopicById(Long.parseLong(topicIdLbl.getText()));
        topic.setTopicName(topicNameTxt.getText());
        topic.setDate(topicDate.getValue());
        topicDao.updateTopic(topic);

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Topic successfully updated!");
        alert.showAndWait();
        displayInfoTopic();
    }

    //UPDATE TRAINER
    @FXML
    private void updateTrainer(){
        Trainer trainer = trainerDao.readTrainerById(Long.parseLong(trainerIdLbl.getText()));
        trainer.setModule(moduleDao.readModuleById(Long.parseLong(moduleIdLbl.getText())));
        trainer.setCertificate(certificateCmb.getValue());
        trainer.setStatus(trainerStatusCmb.getValue());
        trainer.setPhone(trainerPhoneTxt.getText());
        trainer.setPassword(trainerPassword.getText());
        trainer.setEmail(trainerEmailTxt.getText());
        trainer.setLastName(trainerLastNameTxt.getText());
        trainer.setFirstName(trainerFirstNameTxt.getText());

        trainerDao.updateTrainer(trainer);
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Trainer successfully update!");
        alert.showAndWait();
        displayInfoTrainer();

    }

    //DELETE TRAINER
    @FXML
    private void deleteTrainer(){
        trainerDao.deleteTrainer(Long.parseLong(trainerIdLbl.getText()));
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Trainer successfully deleted!");
        alert.showAndWait();
        displayInfoTrainer();
    }

    //UPDATE COURSE
    @FXML
    private void updateCourse(){
        Course course = courseDao.readCourseById(Long.parseLong(courseIdLbl.getText()));
        course.setCourseName(courseNameTxt.getText());
        course.setBeginDate(beginDatePicker.getValue());
        course.setEndingDate(endDatePicker.getValue());
        courseDao.updateCourse(course);

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Course successfully updated!");
        alert.showAndWait();
        displayInfoCourse();
        displayInfoStudent();
    }

    //DELETE COURSE
    @FXML
    private void deleteCourse(){
        courseDao.deleteCourse(Long.parseLong(courseIdLbl.getText()));
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Course successfully deleted!");
        alert.showAndWait();
        displayInfoCourse();
        displayInfoStudent();
    }

    public static void displayInfoTopic(){
        System.out.println("------------INFO TOPIC------------- ");
        for(Topic topic: topicDao.readAllTopic()){
            topic.displayInfoTopics();
            System.out.println();
        }
        System.out.println("----------------------------");
    }

    public static void displayInfoModule(){
        System.out.println("------------MODULE INFO------------- ");
        for(Topic topic: topicDao.readAllTopic()){
            topic.displayInfoTopics();
            System.out.println();
        }
        System.out.println("----------------------------");
    }

    public static void displayInfoCourse(){
        System.out.println("------------COURSE INFO------------- ");
        for(Course course: courseDao.readAllCourse()){
            course.displayInfoCourse();
            System.out.println();
        }
        System.out.println("----------------------------");
    }

    public static void displayInfoTrainer(){
        System.out.println("-----------------------TRAINER INFO-------------");
        for(Trainer trainer : trainerDao.readAllTrainer()){
            trainer.displayInfoTrainer();
            System.out.println();
        }
        System.out.println("--------------------------------------------");
    }

    public static void displayInfoStudent(){
        System.out.println("----------------STUDENT INFO-----------------------");
        for(Student student : studentDao.readAllStudent()){
            student.displayInfoStudent();
            System.out.println();
        }
        System.out.println("---------------------------------------------");
    }

}


