package com.sda.Gina.sudentRegister.view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.net.URL;

public class StudentRegisterMain extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(new URL("file:C:\\Users\\Gina\\IdeaProjects\\StudentRegister\\src\\main\\java\\com\\sda\\Gina\\sudentRegister\\view\\StudentRegisterMain.fxml"));
            Pane pane = loader.load();
            primaryStage.setScene(new Scene(pane));
            primaryStage.setTitle("Student register application");
            primaryStage.show();
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
