package com.sda.Gina.sudentRegister.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class MaintainerMainController implements Initializable {
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    private void showMaintainerLogin(ActionEvent event){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(new URL("file:C:\\Users\\Gina\\IdeaProjects\\StudentRegister\\src\\main\\java\\com\\sda\\Gina\\sudentRegister\\view\\MaintainerLogin.fxml"));
            Parent loginParent = loader.load();

            Scene scene = new Scene(loginParent);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

            window.setScene(scene);
            window.centerOnScreen();
            window.setTitle("Maintainer login form");
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    @FXML
    private void showCourseData(ActionEvent event){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(new URL("file:C:\\Users\\Gina\\IdeaProjects\\StudentRegister\\src\\main\\java\\com\\sda\\Gina\\sudentRegister\\view\\CourseData.fxml"));
            Parent loginParent = loader.load();

            Scene scene = new Scene(loginParent);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

            window.setScene(scene);
            window.centerOnScreen();
            window.setTitle("Course data information");
        }catch(Exception e){
            e.printStackTrace();
        }

    }
}
