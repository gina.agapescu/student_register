package com.sda.Gina.sudentRegister.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class StudentRegisterLoginController implements Initializable {
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    public void showStudentScene(ActionEvent event){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(new URL("file:C:\\Users\\Gina\\IdeaProjects\\StudentRegister\\src\\main\\java\\com\\sda\\Gina\\sudentRegister\\view\\Student.fxml"));
            Parent loginParent = loader.load();

            Scene scene = new Scene(loginParent);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

            window.setScene(scene);
            window.centerOnScreen();
            window.setTitle("Student attendance and marks");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @FXML
    public void showTrainerScene(ActionEvent event){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(new URL("file:C:\\Users\\Gina\\IdeaProjects\\StudentRegister\\src\\main\\java\\com\\sda\\Gina\\sudentRegister\\view\\Trainer.fxml"));
            Parent loginParent = loader.load();

            Scene scene = new Scene(loginParent);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

            window.setScene(scene);
            window.centerOnScreen();
            window.setTitle("Trainer atttendance and mark");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
