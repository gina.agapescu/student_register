package com.sda.Gina.sudentRegister.model;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "student")
@PrimaryKeyJoinColumn(name = "person_id")
public class Student extends Person {

   @Column(name = "cnp", nullable = false)
   private String studentCnp;

    @Column(name = "profession")
    private String profession;

    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

    @Enumerated(EnumType.STRING)
    @Column(name = "gender", length = 6)
    private Gender gender;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", length = 10)
    private Status status;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "student_course",
            joinColumns = {@JoinColumn(name = "student_id")},
            inverseJoinColumns = {@JoinColumn(name = "course_id")})
    private Set<Course>courseSet;

    @OneToMany(mappedBy = "student", fetch = FetchType.EAGER)
    private Set<Attendance>attendanceSet;

    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
   private Set<Mark> markSet;


    public Student() {
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }


    public Set<Attendance> getAttendanceSet() {
        return attendanceSet;
    }

    public void setAttendanceSet(Set<Attendance> attendanceSet) {
        this.attendanceSet = attendanceSet;
    }

    public Set<Mark> getMarkSet() {
        return markSet;
    }

    public void setMarkSet(Set<Mark> markSet) {
        this.markSet = markSet;
    }

    public Set<Course> getCourseSet() {
        return courseSet;
    }

    public void setCourseSet(Set<Course> courseSet) {
        this.courseSet = courseSet;
    }

    public String getStudentCnp() {
        return studentCnp;
    }

    public void setStudentCnp(String studentCnp) {
        this.studentCnp = studentCnp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        if (!super.equals(o)) return false;
        Student student = (Student) o;
        return dateOfBirth.equals(student.dateOfBirth) &&
                status == student.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), dateOfBirth, status);
    }

    @Override
    public String toString() {
        return "Student{" +
                "studentCnp='" + studentCnp + '\'' +
                ", profession='" + profession + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", gender=" + gender +
                ", status=" + status +
                ", courseSet=" + courseSet +

                "} " + super.toString();
    }

    public void displayInfoStudent(){
        System.out.println("Student id= "+getId());
        System.out.println("User name= "+getUserName());
        System.out.println("Student first name= "+getFirstName());
        System.out.println("Student last name= "+getLastName());
        System.out.println("Student cnp= "+ studentCnp);
        System.out.println("Student email= "+getEmail());
        System.out.println("Student phone= "+getPhone());
        System.out.println("Student password= "+getPassword());
        System.out.println("Profession= "+profession);
        System.out.println("Status= "+status);
        System.out.println("Gender= "+ gender);
        System.out.println("Date of birth= "+dateOfBirth);
        System.out.println("Course= "+courseSet.stream().map(Course::getCourseName).collect(Collectors.toList()));

    }
}
