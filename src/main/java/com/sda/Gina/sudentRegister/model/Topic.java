package com.sda.Gina.sudentRegister.model;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "topic")
public class Topic {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "topic_id")
    private Long topicId;

    @Column(name = "topic_name")
    private String topicName;

    @Column(name = "date")
    private LocalDate date;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "module_id")
    @NotFound(action = NotFoundAction.IGNORE)
    private Module module;


    @OneToMany(mappedBy = "topic", fetch = FetchType.EAGER)
    private Set<Attendance>attendanceSet;

    public Topic() {
    }

    public Long getTopicId() {
        return topicId;
    }

    public void setTopicId(Long topicId) {
        this.topicId = topicId;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }


    public Set<Attendance> getAttendanceSet() {
        return attendanceSet;
    }

    public void setAttendanceSet(Set<Attendance> attendanceSet) {
        this.attendanceSet = attendanceSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Topic)) return false;
        Topic topic = (Topic) o;
        return topicId.equals(topic.topicId) &&
                topicName.equals(topic.topicName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(topicId, topicName);
    }

    @Override
    public String toString() {
        return "Topic{" +
                "topicId=" + topicId +
                ", topicName='" + topicName + '\'' +
                ", date=" + date +
                ", module=" + module +
                '}';
    }

    public void displayInfoTopics(){
        System.out.println("Topics id= "+ topicId);
        System.out.println("Topics name= "+ topicName);
        System.out.println("Topics date= "+date);
        System.out.println("Module= "+module.getModuleName());
       // System.out.println("Students: "+module.getCourse().getStudentSet().stream().map(Student::getLastName).collect(Collectors.toList()));
       //System.out.println("Attendance= "+attendanceSet.stream().map(Attendance::getDate).collect(Collectors.toList()));
    }
}
