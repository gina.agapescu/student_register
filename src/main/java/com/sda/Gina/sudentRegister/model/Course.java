package com.sda.Gina.sudentRegister.model;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "course")
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "course_id")
    private Long courseId;

    @Column(name = "course_name")
    private String courseName;

    @Column(name = "begin_date")
    private LocalDate beginDate;

    @Column(name = "ending_date")
    private LocalDate endingDate;

    @OneToMany(mappedBy = "course", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Module>moduleSet;

    @ManyToMany(mappedBy = "courseSet", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Student>studentSet;

    @OneToMany(mappedBy = "course", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Attendance>attendanceSet;

    @OneToMany(mappedBy = "course", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Mark>markSet;

    public Course() {
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public LocalDate getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(LocalDate beginDate) {
        this.beginDate = beginDate;
    }

    public LocalDate getEndingDate() {
        return endingDate;
    }

    public void setEndingDate(LocalDate endingDate) {
        this.endingDate = endingDate;
    }

    public Set<Module> getModuleSet() {
        return moduleSet;
    }

    public void setModuleSet(Set<Module> moduleSet) {
        this.moduleSet = moduleSet;
    }


    public Set<Student> getStudentSet() {
        return studentSet;
    }

    public void setStudentSet(Set<Student> studentSet) {
        this.studentSet = studentSet;
    }

    public Set<Attendance> getAttendanceSet() {
        return attendanceSet;
    }

    public void setAttendanceSet(Set<Attendance> attendanceSet) {
        this.attendanceSet = attendanceSet;
    }

    public Set<Mark> getMarkSet() {
        return markSet;
    }

    public void setMarkSet(Set<Mark> markSet) {
        this.markSet = markSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Course)) return false;
        Course course = (Course) o;
        return courseId.equals(course.courseId) &&
                courseName.equals(course.courseName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(courseId, courseName);
    }

    @Override
    public String toString() {
        return "Course{" +
                "idCourse=" + courseId +
                ", courseName='" + courseName + '\'' +
                ", beginDate=" + beginDate +
                ", endingDate=" + endingDate +
                ", moduleSet=" + moduleSet +
                '}';
    }

    public void displayInfoCourse(){
        System.out.println("Course id= "+ courseId);
        System.out.println("Course name= "+courseName);
        System.out.println("Begin date= "+beginDate);
        System.out.println("Ending date= "+ endingDate);
        System.out.println("Modules= "+ moduleSet.stream().map(Module::getModuleName).collect(Collectors.toList()));
        System.out.println("Students= "+studentSet.stream().map(Student::getLastName).collect(Collectors.toList()));

    }
}
