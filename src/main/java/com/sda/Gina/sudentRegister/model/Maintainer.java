package com.sda.Gina.sudentRegister.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "maintainer")
@PrimaryKeyJoinColumn(name = "person_id")
public class Maintainer extends Person {

    @Enumerated(EnumType.STRING)
    @Column(name = "status", length = 10)
    private Status status;

    public Maintainer() {
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Maintainer)) return false;
        if (!super.equals(o)) return false;
        Maintainer that = (Maintainer) o;
        return status == that.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), status);
    }

    @Override
    public String toString() {
        return "Maintainer{" +
                "status=" + status +
                "} " + super.toString();
    }

    public void displayInfoMaintainer(){
        System.out.println("Maintainer id= "+ getId());
        System.out.println("Maintainer user name= "+getUserName());
        System.out.println("Maintainer first name= "+getFirstName());
        System.out.println("Maintainer last name= "+getLastName());
        System.out.println("Maintainer email= "+getEmail());
        System.out.println("Maintainer phone= "+ getPhone());
        System.out.println("Status= "+status);
    }

}
