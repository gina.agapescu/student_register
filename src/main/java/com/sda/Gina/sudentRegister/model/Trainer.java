package com.sda.Gina.sudentRegister.model;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "trainer")
@PrimaryKeyJoinColumn(name = "person_id")
public class Trainer extends Person {

    @Column(name = "is_certificate")
    @org.hibernate.annotations.Type(type = "yes_no")
    private boolean isCertificate;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", length = 10, nullable = false)
    private Status status;

    @OneToOne(mappedBy = "trainer", fetch = FetchType.EAGER)
    private Module module;


    public Trainer() {
    }

    public boolean isCertificate() {
        return isCertificate;
    }




    public void setCertificate(boolean certificate) {
        isCertificate = certificate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Trainer)) return false;
        if (!super.equals(o)) return false;
        Trainer trainer = (Trainer) o;
        return status == trainer.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), status);
    }

    @Override
    public String toString() {
        return "Trainer{" +
                "isCertificate=" + isCertificate +
                ", status=" + status +
                ", module=" + module +
                "} " + super.toString();
    }

    public void displayInfoTrainer(){
        System.out.println("Trainer id= "+getId());
        System.out.println("User name= "+getUserName());
        System.out.println("Trainer first name= "+getFirstName());
        System.out.println("Trainer last name= "+ getLastName());
        System.out.println("Trainer email= "+ getEmail());
        System.out.println("Trainer phone= "+ getPhone());
        System.out.println("Trainer status= "+ status);
        System.out.println("Trainer password= "+getPassword());
        System.out.println("Trainer is certificate= "+isCertificate);
        System.out.println("Module= "+module.getModuleName());
    }
}
