package com.sda.Gina.sudentRegister.model;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;
@Entity
@Table(name = "attendance")
public class Attendance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "attendance_id")
    private Long attendanceId;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "is_present")
    @org.hibernate.annotations.Type(type = "yes_no")
    private boolean isPresent;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "student_id")
    @NotFound(action = NotFoundAction.IGNORE)
    private Student student;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "topic_id")
    @NotFound(action = NotFoundAction.IGNORE)
    private Topic topic;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "course_id")
    @NotFound(action = NotFoundAction.IGNORE)
    private Course course;

    public Attendance() {
    }

    public Long getAttendanceId() {
        return attendanceId;
    }

    public void setAttendanceId(Long attendanceId) {
        this.attendanceId = attendanceId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public boolean isPresent() {
        return isPresent;
    }

    public void setPresent(boolean present) {
        isPresent = present;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Attendance)) return false;
        Attendance that = (Attendance) o;
        return attendanceId.equals(that.attendanceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(attendanceId);
    }

    @Override
    public String toString() {
        return "Attendance{" +
                "attendanceId=" + attendanceId +
                ", date=" + date +
                ", isPresent=" + isPresent +
                ", student=" + student +
                ", topic=" + topic +
                '}';
    }

    public void displayInfoAttendance(){
        System.out.println("Attendance id= "+attendanceId);
        System.out.println("Course= "+course.getCourseName());
        System.out.println("Date= "+ date);
        System.out.println("Student= "+student.getFirstName()+" "+student.getLastName());
        System.out.println("Present= "+isPresent);
        System.out.println("Topic= "+topic.getTopicName());

    }
}
