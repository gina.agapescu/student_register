package com.sda.Gina.sudentRegister.model;

public enum Status {
    TRAINER,
    STUDENT,
    MAINTAINER
}
