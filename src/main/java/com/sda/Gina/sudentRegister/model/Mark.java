package com.sda.Gina.sudentRegister.model;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "mark")
public class Mark {
    @Id
    @Column(name = "id_mark")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idMark;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "note")
    private Integer note;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "student_id")
    @NotFound(action = NotFoundAction.IGNORE)
    private Student student;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "module_id")
    @NotFound(action = NotFoundAction.IGNORE)
    private Module module;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "course_id")
    @NotFound(action = NotFoundAction.IGNORE)
    private Course course;

    public Mark() {
    }

    public Long getIdMark() {
        return idMark;
    }

    public void setIdMark(Long idMark) {
        this.idMark = idMark;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Integer getNote() {
        return note;
    }

    public void setNote(Integer note) {
        this.note = note;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Mark)) return false;
        Mark mark = (Mark) o;
        return idMark.equals(mark.idMark);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idMark);
    }

    @Override
    public String toString() {
        return "Mark{" +
                "idMark=" + idMark +
                ", date=" + date +
                ", note=" + note +
                ", student=" + student +
                ", module=" + module +
                '}';
    }

    public void displayInfoMark(){
        System.out.println("Mark id= "+idMark);
        System.out.println("Course= "+course.getCourseName());
        System.out.println("Module= "+module.getModuleName());
        System.out.println("Date= "+date);
        System.out.println("Student: "+student.getFirstName()+" "+student.getLastName()+ " Note; "+ note);


    }
}
