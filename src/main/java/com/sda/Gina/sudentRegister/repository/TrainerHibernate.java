package com.sda.Gina.sudentRegister.repository;

import com.sda.Gina.sudentRegister.model.Maintainer;
import com.sda.Gina.sudentRegister.model.Status;
import com.sda.Gina.sudentRegister.model.Trainer;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TrainerHibernate implements TrainerDao {
    @Override
    public void createTrainer(Trainer trainer) {
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.persist(trainer);
            transaction.commit();
            session.close();
        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public Set<Trainer> readAllTrainer() {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            Set<Trainer>trainerSet = new HashSet<>(session.createQuery("from Trainer", Trainer.class).list());
            session.close();
            return trainerSet;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Trainer readTrainerById(Long id) {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            Trainer trainer = session.find(Trainer.class, id);
            session.close();
            return trainer;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void deleteTrainer(Long id) {
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.remove(readTrainerById(id));
            transaction.commit();
            session.close();
        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public void updateTrainer(Trainer trainer) {
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.merge(trainer);
            transaction.commit();
            session.close();
        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public Trainer readTrainerByNamePasswordAndStatus(String lastName, String password, Status status) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query query= session. createQuery("from Trainer where lastName=:lastName and password=:password and status=:status");
            query.setParameter("lastName", lastName);
            query.setParameter("password", password);
            query.setParameter("status", status);
            Trainer trainer = (Trainer) query.uniqueResult();
            session.close();
            return trainer;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public Trainer readTrainerByFirstAndLastName(String firstName, String lastName) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query query= session. createQuery("from Trainer where firstName=:firstName and lastName=:lastName ");
            query.setParameter("lastName", lastName);
            query.setParameter("firstName", firstName);
            Trainer trainer = (Trainer) query.uniqueResult();
            session.close();
            return trainer;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
