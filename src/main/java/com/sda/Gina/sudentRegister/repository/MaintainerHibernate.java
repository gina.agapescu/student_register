package com.sda.Gina.sudentRegister.repository;

import com.sda.Gina.sudentRegister.model.Maintainer;
import com.sda.Gina.sudentRegister.model.Status;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.HashSet;
import java.util.Set;

public class MaintainerHibernate implements MaintainerDao {
    @Override
    public void createMaintainer(Maintainer maintainer) {
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.persist(maintainer);
            transaction.commit();
            session.close();
        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public Set<Maintainer> readAllMaintainer() {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            Set<Maintainer>maintainerSet = new HashSet<>(session.createQuery("from Maintainer", Maintainer.class).list());
            session.close();
            return maintainerSet;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Maintainer readMaintainerById(Long id) {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            Maintainer maintainer = session.find(Maintainer.class, id);
            session.close();
            return maintainer;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void deleteMaintainer(Long id) {
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.remove(readMaintainerById(id));
            transaction.commit();
            session.close();
        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public void updateMaintainer(Maintainer maintainer) {
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.merge(maintainer);
            transaction.commit();
            session.close();
        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public Maintainer readMaintainerByNamePasswordAndStatus(String name, String password, Status status) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query query= session. createQuery("from Maintainer where name=:name and password=:password and status=:status");
            query.setParameter("name", name);
            query.setParameter("password", password);
            query.setParameter("status", status);
            Maintainer maintainer = (Maintainer) query.uniqueResult();
            session.close();
            return maintainer;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
