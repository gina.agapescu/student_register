package com.sda.Gina.sudentRegister.repository;

import com.sda.Gina.sudentRegister.model.Attendance;
import com.sda.Gina.sudentRegister.model.Course;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.HashSet;
import java.util.Set;

public class AttendanceHibernate implements AttendanceDao {
    @Override
    public void createAttendance(Attendance attendance) {
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.persist(attendance);
            transaction.commit();
            session.close();
        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public Set<Attendance> readAllAttendance() {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            Set<Attendance>attendanceSet = new HashSet<>(session.createQuery("from Attendance", Attendance.class).list());
            session.close();
            return attendanceSet;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Attendance readAttendanceById(Long id) {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            Attendance attendance = session.find(Attendance.class, id);
            session.close();
            return attendance;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public void deleteAttendance(Long id) {
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.remove(readAttendanceById(id));
            transaction.commit();
            session.close();
        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public void updateAttendance(Attendance attendance) {
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.merge(attendance);
            transaction.commit();
            session.close();
        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
}
