package com.sda.Gina.sudentRegister.repository;

import com.sda.Gina.sudentRegister.model.Maintainer;
import com.sda.Gina.sudentRegister.model.Status;

import java.util.Set;

public interface MaintainerDao {
    void createMaintainer(Maintainer maintainer);
    Set<Maintainer> readAllMaintainer();
    Maintainer readMaintainerById(Long id);
    void deleteMaintainer(Long id);
    void updateMaintainer(Maintainer maintainer);
    Maintainer readMaintainerByNamePasswordAndStatus(String name, String password, Status status);
}
