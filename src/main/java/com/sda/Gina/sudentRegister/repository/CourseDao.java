package com.sda.Gina.sudentRegister.repository;

import com.sda.Gina.sudentRegister.model.Course;

import java.util.List;
import java.util.Set;

public interface CourseDao {
    void createCourse(Course course);
    Set<Course> readAllCourse();
    Course readCourseById(Long id);
    void deleteCourse(Long id);
    void updateCourse(Course course);
    List<String>readCourseNames();
    Course readCourseByName(String courseName);
}
