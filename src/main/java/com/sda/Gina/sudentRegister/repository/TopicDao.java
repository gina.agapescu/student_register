package com.sda.Gina.sudentRegister.repository;

import com.sda.Gina.sudentRegister.model.Topic;

import java.util.List;
import java.util.Set;

public interface TopicDao {
    void createTopic(Topic topic);
    Set<Topic> readAllTopic();
    Topic readTopicById(Long id);
    void deleteTopic(Long id);
    void updateTopic(Topic topic);
    Topic readTopicByName(String topicName);
    List<String> readTopicToBindModule(Long idModule);
}
