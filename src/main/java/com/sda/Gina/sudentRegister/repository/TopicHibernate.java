package com.sda.Gina.sudentRegister.repository;

import com.sda.Gina.sudentRegister.model.Module;
import com.sda.Gina.sudentRegister.model.Topic;
import com.sda.Gina.sudentRegister.model.Trainer;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TopicHibernate implements TopicDao {
    @Override
    public void createTopic(Topic topic) {
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.persist(topic);
            transaction.commit();
            session.close();
        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public Set<Topic> readAllTopic() {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            Set<Topic>topicSet = new HashSet<>(session.createQuery("from Topic ", Topic.class).list());
            session.close();
            return topicSet;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Topic readTopicById(Long id) {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            Topic topic = session.find(Topic.class, id);
            session.close();
            return topic;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void deleteTopic(Long id) {
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.remove(readTopicById(id));
            transaction.commit();
            session.close();
        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public void updateTopic(Topic topic) {
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.merge(topic);
            transaction.commit();
            session.close();
        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public Topic readTopicByName(String topicName) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query query= session. createQuery("from Topic where topicName=:topicName ");
            query.setParameter("topicName", topicName);

            Topic topic = (Topic) query.uniqueResult();
            session.close();
            return topic;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<String> readTopicToBindModule(Long idModule) {
        List<String> topicNames= null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query query = session.createQuery("select topicName from Topic where module_id=:moduleId");
            query.setParameter("moduleId", idModule);
            topicNames = query.getResultList();
        }catch(Exception e){
            e.printStackTrace();
        }
        return topicNames;
    }

}
