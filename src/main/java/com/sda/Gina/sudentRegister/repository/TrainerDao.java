package com.sda.Gina.sudentRegister.repository;

import com.sda.Gina.sudentRegister.model.Maintainer;
import com.sda.Gina.sudentRegister.model.Status;
import com.sda.Gina.sudentRegister.model.Trainer;

import java.util.List;
import java.util.Set;

public interface TrainerDao {
    void createTrainer(Trainer trainer);
    Set<Trainer> readAllTrainer();
    Trainer readTrainerById(Long id);
    void deleteTrainer(Long id);
    void updateTrainer(Trainer trainer);
    Trainer readTrainerByNamePasswordAndStatus(String lastName, String password, Status status);
    Trainer readTrainerByFirstAndLastName(String firstName, String lastName);
}
