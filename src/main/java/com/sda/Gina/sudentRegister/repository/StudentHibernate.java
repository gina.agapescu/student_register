package com.sda.Gina.sudentRegister.repository;

import com.sda.Gina.sudentRegister.model.Maintainer;
import com.sda.Gina.sudentRegister.model.Status;
import com.sda.Gina.sudentRegister.model.Student;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class StudentHibernate implements StudentDao {
    @Override
    public void createStudent(Student student) {
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.persist(student);
            transaction.commit();
            session.close();
        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public Set<Student> readAllStudent() {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            Set<Student>studentSet = new HashSet<>(session.createQuery("from Student", Student.class).list());
            session.close();
            return studentSet;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Student readStudentById(Long id) {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            Student student = session.find(Student.class, id);
            session.close();
            return student;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void deleteStudent(Long id) {
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.remove(readStudentById(id));
            transaction.commit();
            session.close();
        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public void updateStudent(Student student) {
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.merge(student);
            transaction.commit();
            session.close();
        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public Student readStudentByNamePasswordAndStatus(String name, String password, Status status) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query query= session. createQuery("from Student where lastName=:lastName and password=:password and status=:status");
            query.setParameter("name", name);
            query.setParameter("password", password);
            query.setParameter("status", status);
            Student student = (Student) query.uniqueResult();
            session.close();
            return student;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Student readStudentByLastName(String lastName) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query query= session. createQuery("from Student where  lastName=:lastName ");
            query.setParameter("lastName", lastName);
            Student student = (Student) query.uniqueResult();
            session.close();
            return student;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Student readStudentByFirstNameAndLastName(String firstName, String lastName) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query query= session. createQuery("from Student where firstName=:firstName and lastName=:lastName ");
            query.setParameter("firstName", firstName);
            query.setParameter("lastName", lastName);
            Student student = (Student) query.uniqueResult();
            session.close();
            return student;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<String> readStudentLastNameForComboBox(Long idCourse) {
        List<String> studentLastNames= null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query query = session.createQuery("select s.lastName from Student s INNER JOIN s.courseSet courses where courses.courseId=:courseId");
            query.setParameter("courseId", idCourse);
            studentLastNames = query.getResultList();
        }catch(Exception e){
            e.printStackTrace();
        }
        return studentLastNames;
    }

    @Override
    public List<String> readStudentFirstNameForComboBox(Long idCourse) {
        List<String> studentFirstNames= null;
        try{

            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query query = session.createQuery("select s.firstName from Student s INNER JOIN s.courseSet courses where courses.courseId=:courseId");
            query.setParameter("courseId", idCourse);
            studentFirstNames = query.getResultList();
        }catch(Exception e){
            e.printStackTrace();
        }
        return studentFirstNames;
    }
}
