package com.sda.Gina.sudentRegister.repository;

import com.sda.Gina.sudentRegister.model.Mark;
import com.sda.Gina.sudentRegister.model.Module;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.HashSet;
import java.util.Set;

public class MarkHibernate implements MarkDao {
    @Override
    public void createMark(Mark mark) {
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.persist(mark);
            transaction.commit();
            session.close();
        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public Set<Mark> readAllMark() {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            Set<Mark>markSet = new HashSet<>(session.createQuery("from Mark", Mark.class).list());
            session.close();
            return markSet;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Mark readMarkById(Long id) {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            Mark mark = session.find(Mark.class, id);
            session.close();
            return mark;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void deleteMark(Long id) {
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.remove(readMarkById(id));
            transaction.commit();
            session.close();
        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public void updateMark(Mark mark) {
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.merge(mark);
            transaction.commit();
            session.close();
        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
}
