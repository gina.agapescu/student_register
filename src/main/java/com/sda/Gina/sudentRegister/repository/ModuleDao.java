package com.sda.Gina.sudentRegister.repository;

import com.sda.Gina.sudentRegister.model.Module;
import com.sda.Gina.sudentRegister.model.Topic;
import com.sda.Gina.sudentRegister.model.Trainer;

import java.util.List;
import java.util.Set;

public interface ModuleDao {
    void createModule(Module module);
    Set<Module> readAllModule();
    Module readModuleById(Long id);
    void deleteModule(Long id);
    void updateModule(Module module);
    Module readModuleByName(String moduleName);
    List<String>readModuleToBindCourse(Long idCourse);
    List<String>readTrainerFirstName(Long moduleId);
    List<String>readTrainerLastName(Long moduleId);
    Trainer findTrainerByModule(Long moduleId);
}
