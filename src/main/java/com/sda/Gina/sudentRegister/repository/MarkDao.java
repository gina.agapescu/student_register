package com.sda.Gina.sudentRegister.repository;

import com.sda.Gina.sudentRegister.model.Mark;

import java.util.Set;

public interface MarkDao {
    void createMark(Mark mark);
    Set<Mark> readAllMark();
    Mark readMarkById(Long id);
    void deleteMark(Long id);
    void updateMark(Mark mark);

}
