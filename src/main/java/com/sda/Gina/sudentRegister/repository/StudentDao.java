package com.sda.Gina.sudentRegister.repository;

import com.sda.Gina.sudentRegister.model.Status;
import com.sda.Gina.sudentRegister.model.Student;

import java.util.List;
import java.util.Set;

public interface StudentDao {
    void createStudent(Student student);
    Set<Student> readAllStudent();
    Student readStudentById(Long id);
    void deleteStudent(Long id);
    void updateStudent(Student student);
    Student readStudentByNamePasswordAndStatus(String name, String password, Status status);
    Student readStudentByFirstNameAndLastName(String firstName, String lastName);
    Student readStudentByLastName(String lastName);
    List<String>readStudentFirstNameForComboBox(Long idCourse);
    List<String>readStudentLastNameForComboBox(Long idCourse);
}
