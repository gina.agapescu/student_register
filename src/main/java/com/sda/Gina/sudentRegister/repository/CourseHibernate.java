package com.sda.Gina.sudentRegister.repository;

import com.sda.Gina.sudentRegister.model.Course;
import com.sda.Gina.sudentRegister.model.Maintainer;
import com.sda.Gina.sudentRegister.model.Student;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CourseHibernate implements CourseDao {
    @Override
    public void createCourse(Course course) {
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.persist(course);
            transaction.commit();
            session.close();
        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public Set<Course> readAllCourse() {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            Set<Course>courseSet = new HashSet<>(session.createQuery("from Course", Course.class).list());
            session.close();
            return courseSet;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Course readCourseById(Long id) {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            Course course = session.find(Course.class, id);
            session.close();
            return course;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void deleteCourse(Long id) {
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.remove(readCourseById(id));
            transaction.commit();
            session.close();
        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public void updateCourse(Course course) {
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.merge(course);
            transaction.commit();
            session.close();
        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public List<String> readCourseNames() {
        List<String> courseNames= null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query query = session.createQuery("select courseName from Course ");
            courseNames = query.getResultList();
        }catch(Exception e){
            e.printStackTrace();
        }
        return courseNames;
    }

    @Override
    public Course readCourseByName(String courseName) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query query= session. createQuery("from Course where courseName=:courseName ");
            query.setParameter("courseName", courseName);
            Course course = (Course) query.uniqueResult();
            session.close();
            return course;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
