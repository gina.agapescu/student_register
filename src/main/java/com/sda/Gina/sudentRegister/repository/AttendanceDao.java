package com.sda.Gina.sudentRegister.repository;

import com.sda.Gina.sudentRegister.model.Attendance;
import com.sda.Gina.sudentRegister.model.Course;

import java.util.Set;

public interface AttendanceDao {
    void createAttendance(Attendance attendance);
    Set<Attendance> readAllAttendance();
    Attendance readAttendanceById(Long id);
    void deleteAttendance(Long id);
    void updateAttendance(Attendance attendance);
}
