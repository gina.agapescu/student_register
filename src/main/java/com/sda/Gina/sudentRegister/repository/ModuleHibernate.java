package com.sda.Gina.sudentRegister.repository;

import com.sda.Gina.sudentRegister.model.Course;
import com.sda.Gina.sudentRegister.model.Module;
import com.sda.Gina.sudentRegister.model.Topic;
import com.sda.Gina.sudentRegister.model.Trainer;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ModuleHibernate implements ModuleDao {
    @Override
    public void createModule(Module module) {
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.persist(module);
            transaction.commit();
            session.close();
        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public Set<Module> readAllModule() {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            Set<Module>moduleSet = new HashSet<>(session.createQuery("from Module", Module.class).list());
            session.close();
            return moduleSet;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Module readModuleById(Long id) {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            Module module = session.find(Module.class, id);
            session.close();
            return module;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void deleteModule(Long id) {
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.remove(readModuleById(id));
            transaction.commit();
            session.close();
        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public void updateModule(Module module) {
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.merge(module);
            transaction.commit();
            session.close();
        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public Module readModuleByName(String moduleName) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query query= session. createQuery("from Module where moduleName=:moduleName ");
            query.setParameter("moduleName", moduleName);

            Module module = (Module) query.uniqueResult();
            session.close();
            return module;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<String> readModuleToBindCourse(Long idCourse) {
        List<String> moduleNames= null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query query = session.createQuery("select moduleName from Module where course_id=:courseId");
            query.setParameter("courseId", idCourse);
            moduleNames = query.getResultList();
        }catch(Exception e){
            e.printStackTrace();
        }
        return moduleNames;
    }

    @Override
    public List<String> readTrainerFirstName(Long moduleId) {
        List<String>trainerFirstName = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query query = session.createQuery("select trainer.firstName from Module where moduleId=:moduleId");
            query.setParameter("moduleId", moduleId);
            trainerFirstName = query.getResultList();
        }catch(Exception e){
            e.printStackTrace();
        }
        return trainerFirstName;
    }

    @Override
    public List<String> readTrainerLastName(Long moduleId) {
        List<String>trainerLastName = null;
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query query = session.createQuery("select trainer.lastName from Module where moduleId=:moduleId");
            query.setParameter("moduleId", moduleId);
            trainerLastName = query.getResultList();
        }catch(Exception e){
            e.printStackTrace();
        }
        return trainerLastName;
    }

    @Override
    public Trainer findTrainerByModule(Long moduleId) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query query= session. createQuery("Select m.trainer from Module m where m.moduleId=:moduleId ");
            query.setParameter("moduleId", moduleId);

            Trainer trainer = (Trainer) query.uniqueResult();
            session.close();
            return trainer;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
